# LM Sprint Tool

Welcom to the Logicmedia sprint tool.

#### Features

-   Current burndown chart
-   Monte Carlo forecast
-   PBI leadtime
-   PBI cycle time
-   Cumulative flow chart
-   Bug pie (WIP)

This tool can generate a burndown chart for a given team or it can generate
a monto carlo forecast chart for a team, backlog and sprint length.

## Usage

### Authorization

#### DevOps

To get access to Azure DevOps we need a Personal Access Token (PAT) from your
DevOps account <https://dev.azure.com/logicmedia365/_usersSettings/tokens>.

#### Toggl

And you'll need to get an `api_token` from Toggl: You can find it here:
<https://track.toggl.com/profile> and scroll down to the bottom where i says
`API Token`.

### General usage

You can just run the application and it will guide you through the
authorization. This is only requeired the first time you run the application.

And when that is completed, you'll be able to generate chart for every team.

Everything else below is focused on explaining the kind of chart and how to
generate a chart from the command line, ie. if you wanted to automate some of
the chart generation.

### Generate burndown

To generate a SVG burndownchart for Team Wilhelm execute the following command

    sprint_tool.exe bc Wilhelm

This will drop a SVG in the same directory as the executable

To get burndown chart for an old sprint run this command

    sprint_tool.exe oldbc ApplePie "Sprint 010"

### Generate Monte Carlo chart

This chart is used to forecast how much work a team can do in a fixed time
frame based on historic data.

To generate a SVG Monte Carlo estimation for Team Just Works execute the
following command:

    sprint_tool.exe mc "Just Works" 100 10

What is happening here? The command will generate an Monte Carlo forecast based
on how the team have previous performed. The value `100` is the backlog size
that we want an forecast for and `10` is the sprint length we want to base our
forecast on. The forecast will be most precise when there is a larger number of
sprints with the same length.

This will drop a SVG in the same directory as the executable

### Generate Cumulative Flow Diagram

You can generate a Cumulative Flow Diagram (CFD) to track how work accumulating
over time. For example you want to track how bugs are getting handled over time
and to see if there were any spikes where a lot of new work were added. It will
only look at how created work in that periode has evolved. Work created before
this periode wil not be taken into account.

Lets say you want to generate a bug cfd for the first quarter of 2020 for
ApplePie you would run this command:

    sprint_tool.exe cfd ApplePie bug 20Q1

Supported chart types are: `userstory`, `bug`, `issue` and `task`.

The supported short hand for periods are:

-   Get first or second hald of year: `YYH1` or `YYH2`
-   Get a quarter of the year: `YYQ{Quarter number}`, fx. `19Q3` will get you the
    period from 1st of July to 30th of September 2019.
-   Get a month of the year: `YYM{Month number}`, fx. `19M10` will get you the
    period from the 1st of October to the 31st of October.
    `Month number` start at 1 and ends at 12,

**Known issues**

-   You cannot generate a cfd before second quater of 2020. The data is not there
    to generate a meaningful cfd.

### Generate PBI aging plot

You can generate a scatter plot diagram, which shows how old non-started work
items are.

This is particular useful for discovering old work items, which we have
forgotten.

Lets say you want to generate a user story pbi age plot for ApplePie you would
run this command:

    sprint_tool.exe pbiage ApplePie userstory

Supported chart types are: `userstory`, `bug`, `issue` and `task`.

### Generate PBI lead time plot

You can generate a scatter plot diagram, whichs shows how long time it took for
a work item to be completed from the time it were created. This is do identify
how long time it takes for work to be registered in the system and to when it
is completed. This could be useful for tracking how long it takes bugs to be
closed from the day it is created.

This is particular useful for a PO when we want to forecast when task might be
done.

    sprint_tool.exe leadtime Wilhelm userstory

Supported chart types are: `userstory`, `bug`, `issue` and `task`.

## Further Improvements

Suggestions to new features:

-   Show how long time it takes for a bug to change state depending on priority.
-   Draw plot scatter for cycle time and lead time of bugs and user stories.
    -   Draw bugs in color of priority.
-   Track bugs in a sprint and their priority.
    -   New vs closed
-   Track re-opened bugs
-   CFD with new, open and closed bugs per date or sprint
-   Show a list of work items along with the scatter plot
-   Scatter plot with idle time on user stories before work is started on the
    item
-   Throughput graph over completed user stories in a certain timeframe
-   Complete the bug pie

## Limitations

-   You cannot change where the SVG chart is saved.
-   It only supports SVG chart generation.

## Contribution

### Requirements

1. First install Rust from <https://www.rust-lang.org/> and follow the
   installation guide.
2. The computer running the executable needs to be able to reach
   <https://dev.azure.com/logicmedia365> otherwise it will fail.

### Build

1. Run `cargo build` and it wil generate an exe in `target\debug\sprint_tool.exe`

-   If you add the `--release` param it will generate a release build in
    `target\release\sprint_tool.exe`

The executable is selfcontained and has no runtime dependencies once it is
compiled and therefor you can place the executable anywhere on your system and
run it.

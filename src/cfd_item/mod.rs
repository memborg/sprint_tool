use crate::period;
use crate::work_items::*;
use chrono::prelude::*;

#[derive(Debug, Clone)]
pub struct CFDItem {
    pub date: Option<Date<Utc>>,
    pub count: i32,
}

#[derive(Debug, Clone)]
pub struct CFDCollection {
    pub lead_time: Vec<CFDItem>,
    pub cycle_time: Vec<CFDItem>,
    pub resolved: Vec<CFDItem>,
    pub closed: Vec<CFDItem>,
    pub work_items: Vec<WorkItem>,
    pub period: period::Period,
}

impl Default for CFDCollection {
    fn default() -> CFDCollection {
        CFDCollection {
            lead_time: Vec::new(),
            cycle_time: Vec::new(),
            resolved: Vec::new(),
            closed: Vec::new(),
            work_items: Vec::new(),
            period: period::Period::new(),
        }
    }
}

impl CFDCollection {
    pub fn new() -> CFDCollection {
        CFDCollection::default()
    }
}

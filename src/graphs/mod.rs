use std::fmt;

pub mod bugpie;
pub mod burndown;
pub mod cfd;
pub mod monte_carlo;
pub mod pbiage;
pub mod pbileadtime;

#[derive(Clone, Debug)]
pub struct Graph {
    start: String,
    head: String,
    pub middle: String,
    end: String,
    pub canvas_height: usize,
    pub canvas_width: usize,
}

#[derive(Clone, Debug)]
pub enum GraphSource {
    DevOps,
    Toggl,
}

#[derive(Clone, Debug)]
pub struct GraphType {
    pub key: String,
    pub name: String,
    pub source: GraphSource,
}

impl Graph {
    pub fn new(canvas_height: usize, canvas_width: usize) -> Graph {
        Graph {
            start: get_start(canvas_height, canvas_width),
            head: get_head(),
            middle: String::new(),
            end: get_end(),
            canvas_height,
            canvas_width,
        }
    }

    pub fn add_y_axis(&mut self, chart_height: f64, ratio: f64, unit: &str) {
        let mut y_start = 0.0;
        let y_max = 50.0;
        let y_tick_size = 50.0;
        let y_ticks = y_tick_size;
        let mut y_pos = chart_height;

        self.middle.push_str("<g class=\"y axis\">");
        loop {
            self.middle.push_str(
                format!(
                    "<g class=\"tick\" transform=\"translate(0,{})\" style=\"opacity: 1;\">",
                    y_pos
                )
                .as_str(),
            );
            self.middle.push_str("<line x2=\"-6\" y2=\"0\"></line>");
            self.middle.push_str(
                "<text dy=\".32em\" x=\"-9\" y=\"0\" fill=\"#6f6f6f\" text-anchor=\"end\">",
            );
            self.middle
                .push_str(format!("{}", (y_start / ratio) as usize).as_str());
            self.middle.push_str("</text>");
            self.middle.push_str("</g>");

            y_pos -= y_ticks;
            y_start += y_ticks;

            if y_pos <= y_max {
                break;
            }
        }

        self.middle.push_str(
            format!(
                "<path class=\"domain\" d=\"M-3,0H0V{}H-3\" fill=\"#dadada\"></path>",
                chart_height
            )
            .as_str(),
        );

        self.middle
            .push_str("<text y=\"50\" x=\"-27\" fill=\"#6f6f6f\">");
        self.middle.push_str(unit);
        self.middle.push_str("</text>");

        self.middle.push_str("</g>");
    }
}

impl fmt::Display for Graph {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut svg: String = String::new();
        svg.push_str(&self.start);
        svg.push_str(&self.head);
        svg.push_str(&self.middle);
        svg.push_str(&self.end);

        write!(f, "{}", svg)
    }
}

pub fn get_graph_types() -> Vec<GraphType> {
    vec![
        GraphType {
            key: "bc".into(),
            name: "Current iteration".into(),
            source: GraphSource::DevOps,
        },
        GraphType {
            key: "cfd".into(),
            name: "Cumulative flow diagram".into(),
            source: GraphSource::DevOps,
        },
        GraphType {
            key: "mc".into(),
            name: "Monte Carlo forecast".into(),
            source: GraphSource::DevOps,
        },
        GraphType {
            key: "oldbc".into(),
            name: "Previous iterations".into(),
            source: GraphSource::DevOps,
        },
        GraphType {
            key: "pbiage".into(),
            name: "PBI cycletime".into(),
            source: GraphSource::DevOps,
        },
        GraphType {
            key: "leadtime".into(),
            name: "PBI leadtime".into(),
            source: GraphSource::DevOps,
        },
        GraphType {
            key: "bp".into(),
            name: "Toggl Bug Pie".into(),
            source: GraphSource::Toggl,
        },
    ]
}

pub fn get_graph_type(types: Vec<GraphType>, i: usize) -> GraphType {
    types[i].clone()
}

impl fmt::Display for GraphType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

fn get_start(canvas_height: usize, canvas_width: usize) -> String {
    let mut svg: String = String::new();
    svg.push_str("<?xml version=\"1.0\" standalone=\"yes\"?>");
    svg.push_str("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">");
    svg.push_str(
        format!(
            "<svg width=\"{}\" height=\"{}\" xmlns=\"http://www.w3.org/2000/svg\">",
            canvas_width, canvas_height
        )
        .as_str(),
    );

    svg
}

fn get_head() -> String {
    let mut svg: String = String::new();
    svg.push_str("<style>");
    svg.push_str("text { font-family: arial; font-size: 1em; }");
    svg.push_str("text.plot-count { font-family: arial; font-size: 0.55em; }");
    svg.push_str("text.line-text { font-family: arial; font-size: 0.55em; }");
    svg.push_str("</style>");

    svg
}

fn get_end() -> String {
    String::from("</svg>")
}

use crate::config::*;
use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE};

#[derive(Debug, Deserialize)]
pub struct Value {
    pub value: String,
    #[serde(rename = "includeChildren")]
    pub include_children: bool,
}

#[derive(Debug, Deserialize)]
pub struct TeamFieldValues {
    #[serde(rename = "defaultValue")]
    pub default_value: String,
    pub values: Vec<Value>,
    pub url: String,
}

fn construct_headers() -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers
}

pub fn get_team_field_values(url: &str) -> Result<TeamFieldValues, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();

    let url = reqwest::Url::parse(url).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.username, Some(config.password))
        .send()
        .expect("Failed to send request");

    response.json::<TeamFieldValues>()
}

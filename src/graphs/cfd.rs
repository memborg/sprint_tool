use crate::cfd_item::*;
use crate::graphs::*;
use crate::work_items::WorkItemType;
use chrono::prelude::*;
use chrono::Duration;

pub fn create_cfd_svg(team: &str, sub_type: &WorkItemType, mut cfd_col: CFDCollection) -> String {
    let active_color = "#007acc";
    let resolved_color = "#f19500";
    let new_color = "#b2b2b2";
    let closed_color = "#339933";
    let canvas_height = 600;
    let canvas_width = 960;
    let chart_width = 900;
    let chart_height = 450.0;
    let plural = WorkItemType::get_plural_string(sub_type);
    let type_name = WorkItemType::get_string(sub_type);
    let today = Utc::today();

    let mut g = Graph::new(canvas_height, canvas_width);

    g.middle.push_str("<g transform=\"translate(40,20)\">");

    //x axis begin
    g.middle.push_str(
        format!(
            "<g class=\"x axis\" transform=\"translate(0,{})\">",
            chart_height
        )
        .as_str(),
    );

    let mut x_pos: i64 = 0;
    let days = cfd_col.period.days;
    let x_ratio = chart_width / days;
    let mut x_today: i64 = 0;

    for d in 0..days {
        let next_date = cfd_col.period.start.unwrap() + Duration::days(d);

        if next_date == today {
            x_today = x_pos;
        }

        if d % cfd_col.period.modulo == 0 {
            g.middle.push_str(format!(
                "<text transform=\"translate({},50)rotate(290)\" fill=\"#6f6f6f\" text-anchor=\"middle\">",
                x_pos
            ).as_str());
            g.middle
                .push_str(next_date.format("%d-%m-%Y").to_string().as_str());
            g.middle.push_str("</text>");
        }

        x_pos += x_ratio;

        if x_pos >= chart_width {
            break;
        }
    }

    g.middle.push_str(
        format!(
        "<text transform=\"translate({},50)rotate(290)\" fill=\"#6f6f6f\" text-anchor=\"middle\">",
        chart_width
    )
        .as_str(),
    );
    g.middle.push_str(
        cfd_col
            .period
            .end
            .unwrap()
            .format("%d-%m-%Y")
            .to_string()
            .as_str(),
    );
    g.middle.push_str("</text>");

    g.middle.push_str(
        format!(
            "<path class=\"domain\" d=\"M0,3V0H{}V3\" fill=\"#dadada\"></path>",
            chart_width
        )
        .as_str(),
    );

    g.middle.push_str("</g>"); //x axis end

    //y axis start
    let max_count = cfd_col.lead_time.iter().max_by_key(|x| x.count).unwrap();
    let ratio = chart_height / (max_count.count as f64 + 30.0);
    g.add_y_axis(chart_height, ratio, &plural);
    //y axis end

    // Lead time
    let x_ratio = chart_width / cfd_col.lead_time.len() as i64;
    let mut points: Vec<String> = vec![];
    let mut lx = chart_width;
    let mut ly: i32 = chart_height as i32;
    let lx_dist = x_ratio;
    points.push(format!("{},{}", lx, ly));

    cfd_col.lead_time.reverse();

    for l in cfd_col.lead_time {
        ly = chart_height as i32 - (ratio as i32 * l.count);

        points.push(format!("{},{}", lx, ly));

        lx -= lx_dist;
    }

    points.push(format!("{},{}", lx, chart_height));

    g.middle.push_str(
        format!(
            "<polygon points=\"{}\" fill=\"{}\"/>",
            points.join(" "),
            new_color
        )
        .as_str(),
    );

    //Cycle time
    let mut points: Vec<String> = vec![];
    let mut lx = chart_width;
    let mut ly: i32 = chart_height as i32;
    let lx_dist = x_ratio;
    points.push(format!("{},{}", lx, ly));

    cfd_col.cycle_time.reverse();

    for l in cfd_col.cycle_time {
        ly = chart_height as i32 - (ratio as i32 * l.count);

        points.push(format!("{},{}", lx, ly));

        lx -= lx_dist;
    }

    points.push(format!("{},{}", lx, chart_height));

    g.middle.push_str(
        format!(
            "<polygon points=\"{}\" fill=\"{}\"/>",
            points.join(" "),
            active_color
        )
        .as_str(),
    );

    //Resolved
    if sub_type != &WorkItemType::Issue {
        let mut points: Vec<String> = vec![];
        let mut lx = chart_width;
        let mut ly: i32 = chart_height as i32;
        let lx_dist = x_ratio;
        points.push(format!("{},{}", lx, ly));

        cfd_col.resolved.reverse();

        for l in cfd_col.resolved {
            ly = chart_height as i32 - (ratio as i32 * l.count);

            points.push(format!("{},{}", lx, ly));

            lx -= lx_dist;
        }

        points.push(format!("{},{}", lx, chart_height));

        g.middle.push_str(
            format!(
                "<polygon points=\"{}\" fill=\"{}\"/>",
                points.join(" "),
                resolved_color
            )
            .as_str(),
        );
    }

    //Closed
    let mut points: Vec<String> = vec![];
    let mut lx = chart_width;
    let mut ly: i32 = chart_height as i32;
    let lx_dist = x_ratio;
    points.push(format!("{},{}", lx, ly));

    cfd_col.closed.reverse();

    for l in cfd_col.closed {
        ly = chart_height as i32 - (ratio as i32 * l.count);

        points.push(format!("{},{}", lx, ly));

        lx -= lx_dist;
    }

    points.push(format!("{},{}", lx, chart_height));

    g.middle.push_str(
        format!(
            "<polygon points=\"{}\" fill=\"{}\"/>",
            points.join(" "),
            closed_color
        )
        .as_str(),
    );

    let avg_lt: i64 = cfd_col
        .work_items
        .iter()
        .map(|x| x.fields.lead_time)
        .sum::<i64>()
        / cfd_col.work_items.len() as i64;

    let avg_ct: i64 = cfd_col
        .work_items
        .iter()
        .map(|x| x.fields.cycle_time)
        .sum::<i64>()
        / cfd_col.work_items.len() as i64;

    g.middle
        .push_str("<text y=\"5\" x=\"50\" fill=\"#6f6f6f\" text-anchor=\"right\">");
    g.middle.push_str(format!("Avg LT: {}h", avg_lt).as_str());
    g.middle.push_str("</text>");

    g.middle
        .push_str("<text y=\"30\" x=\"50\" fill=\"#6f6f6f\" text-anchor=\"right\">");
    g.middle.push_str(format!("Avg CT: {}h", avg_ct).as_str());
    g.middle.push_str("</text>");

    //The today line
    g.middle.push_str(
        format!(
            "<line class=\"today\" x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"0\" stroke=\"#ff0000\"></line>",
            x_today, chart_height, x_today
        )
        .as_str(),
    );

    g.middle.push_str(
        format!(
            "<circle class=\"lead-time\" r=\"5\" cx=\"890\" cy=\"0\" fill=\"{}\"/>",
            new_color
        )
        .as_str(),
    );
    g.middle
        .push_str("<text y=\"5\" x=\"757\" fill=\"#6f6f6f\" text-anchor=\"right\">");
    g.middle.push_str(format!("New {}", plural).as_str());
    g.middle.push_str("</text>");

    g.middle.push_str(
        format!(
            "<circle class=\"cycle-time\" r=\"5\" cx=\"890\" cy=\"25\" fill=\"{}\"/>",
            active_color
        )
        .as_str(),
    );
    g.middle
        .push_str("<text y=\"30\" x=\"745\" fill=\"#6f6f6f\" text-anchor=\"right\">");
    g.middle.push_str(format!("Active {}", plural).as_str());
    g.middle.push_str("</text>");

    if sub_type != &WorkItemType::Issue {
        g.middle.push_str(
            format!(
                "<circle class=\"closed\" r=\"5\" cx=\"890\" cy=\"50\" fill=\"{}\"/>",
                resolved_color
            )
            .as_str(),
        );
        g.middle
            .push_str("<text y=\"55\" x=\"723\" fill=\"#6f6f6f\" text-anchor=\"right\">");
        g.middle.push_str(format!("Resolved {}", plural).as_str());
        g.middle.push_str("</text>");
    }

    g.middle.push_str(
        format!(
            "<circle class=\"resolved\" r=\"5\" cx=\"890\" cy=\"75\" fill=\"{}\"/>",
            closed_color
        )
        .as_str(),
    );
    g.middle
        .push_str("<text y=\"80\" x=\"740\" fill=\"#6f6f6f\" text-anchor=\"right\">");
    g.middle.push_str(format!("Closed {}", plural).as_str());
    g.middle.push_str("</text>");

    //Team name + type + period
    g.middle.push_str(
        format!(
            "<text y=\"565\" x=\"{}\" fill=\"#6f6f6f\" text-anchor=\"middle\" style=\"font-size: 1.5em\">",
            (chart_width as f64 / 2.0).round()
        )
        .as_str(),
    );
    g.middle
        .push_str(format!("{} - {} - {}", team, type_name, cfd_col.period.name).as_str());
    g.middle.push_str("</text>");

    g.middle.push_str("</g>");

    format!("{}", g)
}

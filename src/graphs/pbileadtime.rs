use crate::graphs::*;
use crate::plot::*;
use crate::work_items::WorkItemType;

pub fn create_pbileadtime_svg(team: &str, plot: &PBILeadTimePlot) -> String {
    let canvas_height = 600;
    let canvas_width = 1100;
    let chart_width = 900;
    let chart_height = 450.0;
    let plural = WorkItemType::get_plural_string(&plot.sub_type);
    let type_name = WorkItemType::get_string(&plot.sub_type);

    let mut g = Graph::new(canvas_height, canvas_width);

    g.middle.push_str("<g transform=\"translate(40,20)\">");

    //x axis begin
    g.middle.push_str(
        format!(
            "<g class=\"x axis\" transform=\"translate(0,{})\">",
            chart_height
        )
        .as_str(),
    );

    g.middle.push_str(
        format!(
            "<path class=\"domain\" d=\"M0,3V0H{}V3\" fill=\"#dadada\"></path>",
            chart_width
        )
        .as_str(),
    );

    g.middle.push_str("</g>"); //x axis end

    //y axis start
    let max_count = plot.value.iter().max_by_key(|x| x.age).unwrap();
    let ratio = chart_height / (max_count.age as f64 + 30.0);
    g.add_y_axis(chart_height, ratio, "Age (days)");
    //y axis end

    for l in &plot.lines {
        let y = chart_height - (l.age as f64 * ratio);

        g.middle.push_str(
            format!(
                "<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" stroke=\"#dadada\" stroke-dasharray=\"3\" />",
                0, y, chart_width, y
            )
            .as_str(),
        );

        g.middle.push_str(
            format!(
                "<text class=\"line-text\" x=\"{}\" y=\"{}\" fill=\"#aaaaaa\">{}% ({} days)</text>",
                chart_width + 8,
                y + 3.0,
                l.num,
                l.age
            )
            .as_str(),
        );

        g.middle.push_str(
            format!(
                "<text class=\"line-text\" x=\"{}\" y=\"{}\" fill=\"#aaaaaa\">{}/{} PBIs</text>",
                chart_width + 8,
                y + 14.0,
                l.count,
                l.total
            )
            .as_str(),
        );
    }

    //plotting
    let x_ratio = chart_width as f64 / plot.period.days as f64;

    for p in &plot.value {
        let x = p.doy as f64 * x_ratio;
        let y = chart_height - (p.age as f64 * ratio);
        g.middle.push_str(
            format!(
                "<circle cx=\"{}\" cy=\"{}\" r=\"7\" fill=\"#32a7e4\"/>",
                x, y
            )
            .as_str(),
        );

        if p.count > 1 && p.count < 10 {
            g.middle.push_str(
                format!(
                    "<text class=\"plot-count\" x=\"{}\" y=\"{}\" fill=\"#fff\">{}</text>",
                    x - 2.0,
                    y + 3.0,
                    p.count
                )
                .as_str(),
            );
        }

        if p.count > 9 {
            g.middle.push_str(
                format!(
                    "<text class=\"plot-count\" x=\"{}\" y=\"{}\" fill=\"#fff\">{}</text>",
                    x - 5.0,
                    y + 3.0,
                    p.count
                )
                .as_str(),
            );
        }
    }

    g.middle
        .push_str("<text y=\"5\" x=\"50\" fill=\"#6f6f6f\" text-anchor=\"right\">");
    g.middle.push_str(
        format!(
            "{}/{} {} completed in this period: {} - {}",
            plot.count,
            plot.total,
            plural.to_lowercase(),
            plot.period.start.unwrap().format("%d-%m-%Y"),
            plot.period.end.unwrap().format("%d-%m-%Y")
        )
        .as_str(),
    );
    g.middle.push_str("</text>");

    //Team name + type
    g.middle.push_str(
        format!(
            "<text y=\"500\" x=\"{}\" fill=\"#6f6f6f\" text-anchor=\"middle\" style=\"font-size: 1.5em\">",
            (chart_width as f64 / 2.0).round()
        )
        .as_str(),
    );
    g.middle
        .push_str(format!("{} - {}", team, type_name).as_str());
    g.middle.push_str("</text>");

    g.middle.push_str("</g>");

    format!("{}", g)
}

use crate::config::*;
use crate::sprints;
use chrono::prelude::*;
use chrono::Duration;
use chrono::{DateTime, Utc};
use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE};

#[derive(Debug, Deserialize, Clone)]
struct DateTimeValue {
    #[serde(rename = "newValue")]
    new_value: Option<DateTime<Utc>>,
}

#[derive(Default, Debug, Deserialize, Clone)]
struct StringValue {
    #[serde(rename = "newValue")]
    new_value: String,
}

#[derive(Debug, Deserialize, Clone)]
struct WorkItemUpdateFields {
    #[serde(rename = "System.IterationPath", default)]
    iteration_path: StringValue,
    #[serde(rename = "System.ChangedDate", default)]
    changed_date: DateTimeValue,
    #[serde(rename = "System.CreatedDate", default)]
    created_date: DateTimeValue,
    #[serde(rename = "Microsoft.VSTS.Common.ActivatedDate", default)]
    activated_date: DateTimeValue,
    #[serde(rename = "Microsoft.VSTS.Common.ResolvedDate", default)]
    resolved_date: DateTimeValue,
    #[serde(rename = "Microsoft.VSTS.Common.ClosedDate", default)]
    closed_date: DateTimeValue,
}

#[derive(Debug, Deserialize, Clone)]
struct WorkItemField {
    fields: WorkItemUpdateFields,
}

#[derive(Debug, Deserialize, Clone)]
struct WorkItemUpdate {
    value: Vec<WorkItemField>,
}

#[derive(Debug, Deserialize, Clone, PartialEq)]
pub enum WorkItemType {
    None,
    Bug,
    Issue,
    #[serde(rename = "User Story")]
    UserStory,
    Task,
}

#[derive(Debug, Deserialize, Clone)]
pub enum State {
    None,
    New,
    Active,
    Resolved,
    Closed,
}

#[derive(Debug, Deserialize, Clone)]
pub struct Fields {
    #[serde(rename = "System.Title")]
    pub title: String,
    #[serde(rename = "Microsoft.VSTS.Scheduling.StoryPoints", default)]
    pub story_points: f64,
    #[serde(rename = "System.WorkItemType")]
    pub work_item_type: WorkItemType,
    #[serde(rename = "Microsoft.VSTS.Common.ClosedDate", default)]
    pub closed_date: Option<DateTime<Utc>>,
    #[serde(rename = "System.CreatedDate", default)]
    pub created_date: Option<DateTime<Utc>>,
    #[serde(rename = "Microsoft.VSTS.Common.ActivatedDate", default)]
    pub activated_date: Option<DateTime<Utc>>,
    #[serde(default)]
    pub resolved_date: Option<DateTime<Utc>>,
    #[serde(rename = "System.State")]
    pub state: State,
    #[serde(default)]
    pub closed_day: i32,
    #[serde(default)]
    pub added_day: i32,
    #[serde(default)]
    pub lead_time: i64,
    #[serde(default)]
    pub cycle_time: i64,
    #[serde(default)]
    pub resolved_time: i64,
    #[serde(rename = "Microsoft.VSTS.Common.Priority")]
    pub priority: i32,
    #[serde(rename = "Logicmedia.VSTS.Common.Priority", default)]
    pub lm_priority: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct WorkItem {
    pub id: i32,
    pub fields: Fields,
}

impl Default for Fields {
    fn default() -> Fields {
        Fields {
            title: String::new(),
            story_points: 0.0,
            work_item_type: WorkItemType::None,
            closed_date: Some(Utc::now()),
            created_date: Some(Utc::now()),
            activated_date: None,
            resolved_date: None,
            state: State::None,
            closed_day: 0,
            added_day: 0,
            lead_time: 0,
            cycle_time: 0,
            resolved_time: 0,
            priority: 3,
            lm_priority: "3. Major".into(),
        }
    }
}

impl Fields {
    pub fn new() -> Fields {
        Fields::default()
    }
}

impl Default for WorkItem {
    fn default() -> WorkItem {
        WorkItem {
            id: 0,
            fields: Fields::new(),
        }
    }
}

impl WorkItem {
    pub fn new() -> WorkItem {
        WorkItem::default()
    }
}

// impl Default for StringValue {
//     fn default() -> StringValue {
//         StringValue {
//             new_value: String::new(),
//         }
//     }
// }

impl Default for DateTimeValue {
    fn default() -> DateTimeValue {
        DateTimeValue {
            new_value: Some(Utc::now()),
        }
    }
}

impl WorkItemType {
    pub fn get_enum(wi_type: &str) -> WorkItemType {
        let t = wi_type.to_lowercase();
        let t = t.replace(" ", "");
        let t = t.as_str();

        match t {
            "bug" => WorkItemType::Bug,
            "issue" => WorkItemType::Issue,
            "userstory" => WorkItemType::UserStory,
            "task" => WorkItemType::Task,
            _ => panic!(
                "Supported work item type is: bug, userstory, task and issue, but you got {}.",
                t
            ),
        }
    }

    pub fn get_string(wi_type: &WorkItemType) -> String {
        match wi_type {
            WorkItemType::Bug => "Bug".into(),
            WorkItemType::Issue => "Issue".into(),
            WorkItemType::UserStory => "User Story".into(),
            WorkItemType::Task => "Task".into(),
            _ => String::new(),
        }
    }

    pub fn get_plural_string(wi_type: &WorkItemType) -> String {
        match wi_type {
            WorkItemType::Bug => "Bugs".into(),
            WorkItemType::Issue => "Issues".into(),
            WorkItemType::UserStory => "User Stories".into(),
            WorkItemType::Task => "Tasks".into(),
            _ => String::new(),
        }
    }
}

fn construct_headers() -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers
}

pub fn get_work_item(team: &str, id: i32, sprint: &sprints::Sprint) -> WorkItem {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let url = reqwest::Url::parse(
        format!(
            "https://dev.azure.com/logicmedia365/Logicmedia/_apis/wit/workitems/{}?api-version=5.1",
            id
        )
        .as_str(),
    )
    .expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.username, Some(config.password))
        .send()
        .expect("Failed to send request");

    let mut work_item = WorkItem::new();
    if let Ok(wi) = response.json::<WorkItem>() {
        work_item.id = wi.id;
        work_item.fields = wi.fields;
        if let Some(closed) = work_item.fields.closed_date {
            work_item.fields.closed_day = get_work_day(sprint.start_date.unwrap(), closed);
        }
    }

    if let Ok(updates) = get_work_item_updates(id) {
        if let Some(added_date) = get_added_date(&updates, team, sprint) {
            work_item.fields.added_day = get_work_day(sprint.start_date.unwrap(), added_date);
        }

        work_item.fields.resolved_date = get_last_resolved_date(&updates);
    }

    if let Some(cd) = work_item.fields.closed_date {
        work_item.fields.lead_time = (cd - work_item.fields.created_date.unwrap()).num_hours();
    } else {
        work_item.fields.lead_time =
            (Utc::now() - work_item.fields.created_date.unwrap()).num_hours();
    }

    if let Some(cd) = work_item.fields.closed_date {
        if let Some(ad) = work_item.fields.activated_date {
            work_item.fields.cycle_time = (cd - ad).num_hours();
        }
    } else if let Some(ad) = work_item.fields.activated_date {
        work_item.fields.cycle_time = (Utc::now() - ad).num_hours();
    }

    if let Some(cd) = work_item.fields.closed_date {
        if let Some(rd) = work_item.fields.resolved_date {
            work_item.fields.resolved_time = (cd - rd).num_hours();
        }
    } else if let Some(rd) = work_item.fields.resolved_date {
        work_item.fields.resolved_time = (Utc::now() - rd).num_hours();
    }

    work_item
}

pub fn get_work_item_simple(id: i32) -> WorkItem {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let url = reqwest::Url::parse(
        format!(
            "https://dev.azure.com/logicmedia365/Logicmedia/_apis/wit/workitems/{}?api-version=5.1",
            id
        )
        .as_str(),
    )
    .expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.username, Some(config.password))
        .send()
        .expect("Failed to send request");

    let mut work_item = WorkItem::new();
    if let Ok(wi) = response.json::<WorkItem>() {
        work_item.id = wi.id;
        work_item.fields = wi.fields;
    }

    if let Ok(updates) = get_work_item_updates(id) {
        work_item.fields.resolved_date = get_last_resolved_date(&updates);
    }

    if let Some(cd) = work_item.fields.closed_date {
        work_item.fields.lead_time = (cd - work_item.fields.created_date.unwrap()).num_hours();
    } else {
        work_item.fields.lead_time =
            (Utc::now() - work_item.fields.created_date.unwrap()).num_hours();
    }

    if let Some(cd) = work_item.fields.closed_date {
        if let Some(ad) = work_item.fields.activated_date {
            work_item.fields.cycle_time = (cd - ad).num_hours();
        }
    } else if let Some(ad) = work_item.fields.activated_date {
        work_item.fields.cycle_time = (Utc::now() - ad).num_hours();
    }

    if let Some(cd) = work_item.fields.closed_date {
        if let Some(rd) = work_item.fields.resolved_date {
            work_item.fields.resolved_time = (cd - rd).num_hours();
        }
    } else if let Some(rd) = work_item.fields.resolved_date {
        work_item.fields.resolved_time = (Utc::now() - rd).num_hours();
    }

    work_item
}

fn get_work_item_updates(id: i32) -> Result<WorkItemUpdate, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let url = reqwest::Url::parse(
        format!(
            "https://dev.azure.com/logicmedia365/Logicmedia/_apis/wit/workitems/{}/updates?api-version=5.1",
            id
        )
        .as_str(),
    )
    .expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.username, Some(config.password))
        .send()
        .expect("Failed to send request");

    response.json::<WorkItemUpdate>()
}

fn get_added_date(
    wi_updates: &WorkItemUpdate,
    team: &str,
    sprint: &sprints::Sprint,
) -> Option<DateTime<Utc>> {
    let path = format!("{}\\{}", team, sprint.name);

    let cloned = wi_updates.value.clone();
    let mut cloned = cloned
        .iter()
        .filter(|x| x.fields.iteration_path.new_value == path)
        .map(|x| x.fields.changed_date.new_value)
        .collect::<Vec<_>>();
    cloned.sort_by(|x, y| x.partial_cmp(y).unwrap());

    if let Some(da) = cloned.get(0) {
        *da
    } else {
        None
    }
}

fn get_last_resolved_date(wi_updates: &WorkItemUpdate) -> Option<DateTime<Utc>> {
    let cloned = wi_updates.value.clone();
    let mut cloned = cloned
        .iter()
        .map(|x| x.fields.resolved_date.new_value)
        .collect::<Vec<_>>();

    cloned.sort_by(|x, y| x.partial_cmp(y).unwrap());

    if let Some(rd) = cloned.get(0) {
        *rd
    } else {
        None
    }
}

fn get_work_day(start_date: DateTime<Utc>, end_date: DateTime<Utc>) -> i32 {
    let mut work_days = 0;
    let real_start_date = (start_date + Duration::days(1)) - Duration::seconds(1);

    let mut i = 0;
    loop {
        let new_date = real_start_date + Duration::days(i);

        if new_date > real_start_date {
            match new_date.weekday() {
                Weekday::Sat => {}
                Weekday::Sun => {}
                _ => work_days += 1,
            }
        }

        if new_date >= end_date {
            break;
        }
        i += 1;
    }

    work_days
}

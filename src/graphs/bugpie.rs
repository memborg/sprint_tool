use crate::graphs::*;
use crate::sprints::Sprint;
use crate::toggl::report_details::DetailsReport;

pub fn create_bp_svg(
    _team: &str,
    _toggl_team: &str,
    _sprint: Sprint,
    report: DetailsReport,
) -> String {
    let canvas_height = 600;
    let canvas_width = 960;
    let _chart_height = 450.0;
    let mut g = Graph::new(canvas_height, canvas_width);
    let _cx = 1.0;
    let _cy = 1.0;
    let mut cumulative_percent = 0.0;

    let _team_text_pos = canvas_width as f64;

    g.start = String::new();
    g.start
        .push_str("<?xml version=\"1.0\" standalone=\"yes\"?>");
    g.start.push_str("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.0//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">");
    g.start
        .push_str("<svg viewBox=\"-1 -1 2 2\" style=\"transform: rotate(-0.25turn)\" xmlns=\"http://www.w3.org/2000/svg\" >");

    g.middle
        .push_str("<circle cx=\"0\" cy=\"0\" r=\"1\" fill=\"yellow\" />");

    let p = get_coordinates_for_percent(cumulative_percent);

    let slice_percent = report.development as f64 / report.total as f64;

    cumulative_percent += slice_percent;

    let p2 = get_coordinates_for_percent(cumulative_percent);

    let mut large_arc_flag = 0.0;

    if slice_percent > 0.5 {
        large_arc_flag = 1.0;
    }

    let m = format!("M {} {}", p.0, p.1);
    let a = format!("A 1 1 0 {} 1 {} {}", large_arc_flag, p2.0, p2.1);
    let l = format!("L {} {}", 0.0, 0.0);

    let path = format!("<path d=\"{} {} {}\" fill=\"{}\" /> ", m, a, l, "red");

    g.middle.push_str(path.as_str());

    let p = get_coordinates_for_percent(cumulative_percent);

    let slice_percent = report.bugs as f64 / report.total as f64;

    cumulative_percent += slice_percent;

    let p2 = get_coordinates_for_percent(cumulative_percent);

    let mut large_arc_flag = 0.0;

    if slice_percent > 0.5 {
        large_arc_flag = 1.0;
    }

    let m = format!("M {} {}", p.0, p.1);
    let a = format!("A 1 1 0 {} 1 {} {}", large_arc_flag, p2.0, p2.1);
    let l = format!("L {} {}", 0.0, 0.0);

    let path = format!("<path d=\"{} {} {}\" fill=\"{}\" /> ", m, a, l, "blue");

    g.middle.push_str(path.as_str());

    let p = get_coordinates_for_percent(cumulative_percent);

    let slice_percent = report.support as f64 / report.total as f64;

    cumulative_percent += slice_percent;

    let p2 = get_coordinates_for_percent(cumulative_percent);

    let mut large_arc_flag = 0.0;

    if slice_percent > 0.5 {
        large_arc_flag = 1.0;
    }

    let m = format!("M {} {}", p.0, p.1);
    let a = format!("A 1 1 0 {} 1 {} {}", large_arc_flag, p2.0, p2.1);
    let l = format!("L {} {}", 0.0, 0.0);

    let path = format!("<path d=\"{} {} {}\" fill=\"{}\" /> ", m, a, l, "green");

    g.middle.push_str(path.as_str());

    let p = get_coordinates_for_percent(cumulative_percent);

    let slice_percent = report.agile as f64 / report.total as f64;

    cumulative_percent += slice_percent;

    let p2 = get_coordinates_for_percent(cumulative_percent);

    let mut large_arc_flag = 0.0;

    if slice_percent > 0.5 {
        large_arc_flag = 1.0;
    }

    let m = format!("M {} {}", p.0, p.1);
    let a = format!("A 1 1 0 {} 1 {} {}", large_arc_flag, p2.0, p2.1);
    let l = format!("L {} {}", 0.0, 0.0);

    let path = format!("<path d=\"{} {} {}\" fill=\"{}\" /> ", m, a, l, "purple");

    g.middle.push_str(path.as_str());

    //Team name + sprint #
    // g.middle.push_str(
    //     format!(
    //         "<text y=\"500\" x=\"{}\" fill=\"#6f6f6f\" text-anchor=\"middle\" style=\"font-size: 1.5em\">",
    //         (team_text_pos / 2.0).round()
    //     )
    //     .as_str(),
    // );
    // g.middle
    //     .push_str(format!("{} - {}", team, sprint.name).as_str());
    // g.middle.push_str("</text>");

    format!("{}", g)
}

fn get_coordinates_for_percent(percent: f64) -> (f64, f64) {
    let x = (2.0 * std::f64::consts::PI * percent).cos();
    let y = (2.0 * std::f64::consts::PI * percent).sin();

    (x, y)
}

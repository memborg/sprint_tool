use chrono::prelude::*;
use chrono::Duration;

#[derive(Debug, Clone)]
pub struct Period {
    pub start: Option<Date<Utc>>,
    pub end: Option<Date<Utc>>,
    pub name: String,
    pub days: i64,
    pub modulo: i64,
}

impl Default for Period {
    fn default() -> Period {
        Period {
            start: None,
            end: None,
            name: String::new(),
            days: 0,
            modulo: 6,
        }
    }
}

impl Period {
    pub fn new() -> Period {
        Period::default()
    }
}

pub fn get_period(period: &str) -> Result<Period, &'static str> {
    let mut y: i32 = 2000;
    let mut start_m = 1;
    let mut end_m = 3;
    let start_d = 1;
    let mut end_d = 31;
    let mut modulo = 6;

    let period = period.to_lowercase();
    let period = period.as_str();

    if let Some(ch) = period.get(0..2) {
        match ch.parse::<i32>() {
            Ok(num) => {
                y += num;

                let mut last = 0;

                if let Some(ch) = period.get(3..5) {
                    if let Ok(num) = ch.parse::<i32>() {
                        last = num;
                    }
                }

                if last == 0 {
                    if let Some(ch) = period.get(3..4) {
                        if let Ok(num) = ch.parse::<i32>() {
                            last = num;
                        }
                    }
                }

                match period.get(2..3).unwrap() {
                    "h" => {
                        let half = last;
                        if half == 1 {
                            start_m = 1;
                            end_m = 6;
                        }

                        if half == 2 {
                            start_m = 7;
                            end_m = 12;
                        }
                        modulo = 12;

                        if half > 2 {
                            return Err("There can only be two halfs in a year");
                        }
                    }
                    "q" => {
                        if last == 1 {
                            start_m = 1;
                            end_m = 3;
                        }

                        if last == 2 {
                            start_m = 4;
                            end_m = 6;
                            end_d = (Utc.ymd(y, end_m + 1, 1) - Duration::days(1)).day()
                        }

                        if last == 3 {
                            start_m = 7;
                            end_m = 9;
                            end_d = (Utc.ymd(y, end_m + 1, 1) - Duration::days(1)).day()
                        }

                        if last == 4 {
                            start_m = 10;
                            end_m = 12;
                        }

                        modulo = 6;

                        if last < 1 {
                            return Err("quarter can not be lower than 1");
                        }

                        if last > 4 {
                            return Err("there can only be 4 quarters in a year");
                        }
                    }
                    "m" => {
                        if last < 1 {
                            return Err("Month number can not be lower than 1");
                        }

                        if last > 12 {
                            return Err("The are only 12 months in a year");
                        }

                        start_m = last;
                        end_m = start_m as u32;
                        if last < 12 {
                            end_d = (Utc.ymd(y, end_m + 1, 1) - Duration::days(1)).day()
                        }

                        if last == 12 {
                            end_d = (Utc.ymd(y + 1, 1, 1) - Duration::days(1)).day()
                        }

                        modulo = 3
                    }
                    _ => return Err("invalid char - period char"),
                }
            }
            Err(_) => return Err("invalid char - year char"),
        }
    }

    let start = Utc.ymd(y, start_m as u32, start_d);
    let end = Utc.ymd(y, end_m, end_d);

    Ok(Period {
        start: Some(start),
        end: Some(end),
        name: period.to_uppercase(),
        days: (end - start).num_days(),
        modulo,
    })
}

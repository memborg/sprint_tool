extern crate savefile;
use dialoguer::{theme::ColorfulTheme, Input, Password};

#[derive(Savefile)]
pub struct Config {
    pub username: String,
    pub password: String,
    pub toggl: String,
}

const CODE: &str = "m3mb0rgR0cks";

pub fn get_config() -> Config {
    match load_file() {
        Ok(c) => Config {
            username: c.username,
            password: c.password,
            toggl: c.toggl,
        },
        Err(e) => panic!("Could not load config data! Error: {}", e),
    }
}

fn load_file() -> Result<Config, savefile::SavefileError> {
    savefile::load_encrypted_file::<Config>("config.bin", 0, CODE)
}

pub fn run_setup() -> std::io::Result<()> {
    if load_file().is_err() {
        let username: String = Input::with_theme(&ColorfulTheme::default())
            .with_prompt("DevOps username")
            .allow_empty(false)
            .interact()?;

        let password: String = Password::with_theme(&ColorfulTheme::default())
            .with_prompt("DevOps Personal Access Token")
            .allow_empty_password(false)
            .interact()?;

        let toggl: String = Password::with_theme(&ColorfulTheme::default())
            .with_prompt("Toggl API Token")
            .allow_empty_password(false)
            .interact()?;

        let config = Config {
            username,
            password,
            toggl,
        };

        savefile::save_encrypted_file("config.bin", 0, &config, CODE).unwrap();
    }

    Ok(())
}

pub fn get_workspace_id() -> usize {
    1105316
}

use crate::config::*;
use chrono::prelude::*;
use chrono::Duration;
use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE};
use std::fmt;

#[derive(Debug, Deserialize)]
pub struct Target {
    pub id: i32,
    pub url: String,
}

#[derive(Debug, Deserialize)]
pub struct WorkItemRelation {
    pub target: Target,
}

#[derive(Debug, Deserialize)]
pub struct Iteration {
    #[serde(rename = "workItemRelations")]
    pub work_item_relations: Vec<WorkItemRelation>,
}

#[derive(Debug, Deserialize, Copy, Clone, Eq, Ord, PartialEq, PartialOrd)]
pub enum TimeFrame {
    None,
    #[serde(rename = "past")]
    Past,
    #[serde(rename = "current")]
    Current,
    #[serde(rename = "future")]
    Future,
}

#[derive(Debug, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd)]
pub struct Attributes {
    #[serde(rename = "startDate")]
    pub start_date: Option<DateTime<Utc>>,
    #[serde(rename = "finishDate")]
    pub finish_date: Option<DateTime<Utc>>,
    #[serde(rename = "timeFrame")]
    pub time_frame: TimeFrame,
    #[serde(default)]
    pub days: i32,
    #[serde(default)]
    pub days_progress: i32,
}

#[derive(Debug, Deserialize, Clone, Eq, Ord, PartialEq, PartialOrd)]
pub struct IterationValue {
    pub attributes: Attributes,
    pub id: String,
    pub name: String,
    pub path: String,
    pub url: String,
}

#[derive(Default, Debug, Deserialize, Eq, Ord, PartialEq, PartialOrd)]
pub struct Iterations {
    pub count: i32,
    pub value: Vec<IterationValue>,
}

impl Default for Attributes {
    fn default() -> Attributes {
        Attributes {
            start_date: None,
            finish_date: None,
            time_frame: TimeFrame::None,
            days: 0,
            days_progress: 0,
        }
    }
}

impl Iterations {
    pub fn new() -> Iterations {
        Iterations::default()
    }
}

impl TimeFrame {
    fn is_past(self) -> bool {
        matches!(self, TimeFrame::Past)
    }

    fn is_current(self) -> bool {
        matches!(self, TimeFrame::Current)
    }
}

impl fmt::Display for IterationValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

fn construct_headers() -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers
}

pub fn get_past_iterations_in_range(team: &str, sl_start: i32, sl_end: i32) -> Iterations {
    let mut iterations = get_interations(team);

    iterations.value = iterations
        .value
        .into_iter()
        .filter(|x| {
            x.attributes.days >= sl_start
                && x.attributes.days <= sl_end
                && x.attributes.time_frame.is_past()
        })
        .collect::<Vec<IterationValue>>();

    iterations.count = iterations.value.len() as i32;

    iterations
}

pub fn get_past_iterations(team: &str, sprint_len: i32) -> Iterations {
    let mut iterations = get_interations(team);

    iterations.value = iterations
        .value
        .into_iter()
        .filter(|x| x.attributes.days == sprint_len && x.attributes.time_frame.is_past())
        .collect::<Vec<IterationValue>>();

    iterations.count = iterations.value.len() as i32;

    iterations
}

pub fn get_iteration_by_i(iterations: &[IterationValue], i: usize) -> IterationValue {
    iterations[i].clone()
}

pub fn get_iteration(team: &str, sprint: &str) -> Iterations {
    let mut iterations = get_interations(team);

    iterations.value = iterations
        .value
        .into_iter()
        .filter(|x| x.name == sprint)
        .collect::<Vec<IterationValue>>();

    iterations.count = iterations.value.len() as i32;

    iterations
}

pub fn get_current_iteration(team: &str) -> Iterations {
    let mut iterations = get_interations(team);

    iterations.value = iterations
        .value
        .into_iter()
        .filter(|x| x.attributes.time_frame.is_current())
        .collect::<Vec<IterationValue>>();

    iterations.count = iterations.value.len() as i32;

    iterations
}

pub fn get_iterations_value(team: &str) -> Vec<IterationValue> {
    get_interations(team).value
}

pub fn get_interations(team: &str) -> Iterations {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let url = reqwest::Url::parse(format!("https://dev.azure.com/logicmedia365/Logicmedia/{}/_apis/work/teamsettings/iterations?api-version=5.1", team).as_str()).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.username, Some(config.password))
        .send()
        .expect("Failed to send request");

    let mut iterations = Iterations::new();

    if let Ok(iters) = response.json::<Iterations>() {
        iterations.count = iters.count;
        for iter in iters.value.iter() {
            let mut duration = 0;
            let mut days_progress = 0;

            if let Some(finish_date) = iter.attributes.finish_date {
                duration = get_work_days(iter.attributes.start_date.unwrap(), finish_date);
                days_progress = get_work_days(
                    iter.attributes.start_date.unwrap(),
                    Utc::today().and_hms(0, 0, 0),
                );
            }

            iterations.value.push(IterationValue {
                attributes: Attributes {
                    start_date: iter.attributes.start_date,
                    finish_date: iter.attributes.finish_date,
                    time_frame: iter.attributes.time_frame,
                    days: duration,
                    days_progress,
                },
                id: iter.id.clone(),
                name: iter.name.clone(),
                path: iter.path.clone(),
                url: iter.url.clone(),
            });
        }
    }

    iterations
}

fn get_work_days(start_date: DateTime<Utc>, end_date: DateTime<Utc>) -> i32 {
    let mut work_days = 0;

    let mut i = 0;
    loop {
        let new_date = start_date + Duration::days(i);

        match new_date.weekday() {
            Weekday::Sat => {}
            Weekday::Sun => {}
            _ => work_days += 1,
        }

        if new_date >= end_date {
            break;
        }
        i += 1;
    }

    work_days
}

pub fn get_iteration_work_items(team: &str, iteration: &str) -> Result<Iteration, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let url = reqwest::Url::parse(format!("https://dev.azure.com/logicmedia365/Logicmedia/{}/_apis/work/teamsettings/iterations/{}/workitems?api-version=5.1-preview.1", team, iteration).as_str()).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.username, Some(config.password))
        .send()
        .expect("Failed to send request");

    response.json::<Iteration>()
}

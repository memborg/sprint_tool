use crate::graphs::*;
use crate::sprints::Sprint;
use crate::tools::*;

pub fn create_bc_svg(team: &str, sprint: Sprint) -> String {
    let canvas_height = 600;
    let canvas_width = 960;
    let chart_height = 450.0;
    let mut g = Graph::new(canvas_height, canvas_width);

    g.middle.push_str("<g transform=\"translate(40,20)\">");

    //x axis begin
    g.middle.push_str(
        format!(
            "<g class=\"x axis\" transform=\"translate(0,{})\">",
            chart_height
        )
        .as_str(),
    );

    let mut x_pos: f64 = -38.5;
    let x_dist: f64 = 64.0;
    for day in 1..=sprint.days {
        x_pos = round_to_two_decimals(x_pos + x_dist);
        g.middle.push_str(
            format!(
                "<g class=\"tick\" transform=\"translate({},0)\" style=\"opacity: 1.0\">",
                x_pos
            )
            .as_str(),
        );
        g.middle.push_str("<line y2=\"6\" x2=\"0\"></line>");
        g.middle.push_str(
            "<text dy=\".71em\" y=\"9\" x=\"0\" fill=\"#6f6f6f\" text-anchor=\"middle\">",
        );
        g.middle.push_str(format!("{}", day).as_str());
        g.middle.push_str("</text>");
        g.middle.push_str("</g>");
    }

    let line_x2 = x_pos;

    x_pos = round_to_two_decimals(x_pos + x_dist).round();

    let team_text_pos = x_pos;

    g.middle.push_str(
        format!(
            "<path class=\"domain\" d=\"M0,3V0H{}V3\" fill=\"#dadada\"></path>",
            x_pos
        )
        .as_str(),
    );

    g.middle.push_str("</g>"); //x axis end

    //y axis start
    let ratio = chart_height / (sprint.story_points + 30.0);
    g.add_y_axis(chart_height, ratio, "SP");
    let line_y2 = chart_height - (sprint.start_story_points * ratio);
    //y axis end

    let x_dist = x_dist as i32;
    let mut rect_x = 10;
    let width = 31;
    let mut closed = 0.0;
    for i in 0..=sprint.days_progress {
        if let Some(p) = sprint.progress.get(i as usize) {
            let t_height = ratio * p.total;
            closed += p.closed;
            let remaining = p.total - closed;
            let c_height = ratio * remaining;

            //Total points
            let rect_y = chart_height - t_height;
            g.middle.push_str(
                format!(
                    "<rect class=\"bar\" x=\"{}\" width=\"{}\" y=\"{}\" height=\"{}\" fill=\"#ffc000\" opacity=\"0.5\">",
                    rect_x, width, rect_y, t_height
                )
                .as_str(),
            );
            g.middle.push_str("</rect>");

            //Closed points
            let rect_y = chart_height - c_height;
            g.middle.push_str(
                format!(
                    "<rect class=\"bar\" x=\"{}\" width=\"{}\" y=\"{}\" height=\"{}\" fill=\"#4472c4\">",
                    rect_x, width, rect_y, c_height
                )
                .as_str(),
            );
            g.middle.push_str("</rect>");

            if remaining > 0.0 {
                g.middle.push_str(
                    format!(
                        "<text y=\"{}\" x=\"{}\" fill=\"#6f6f6f\" style=\"font-size: 0.7em;\">",
                        (rect_y + 5.0),
                        (rect_x + width + 3)
                    )
                    .as_str(),
                );
                g.middle.push_str(format!("{}", remaining).as_str());
                g.middle.push_str("</text>");
            }

            rect_x += x_dist;
        }
    }

    //Projected line
    g.middle.push_str(
        format!(
            "<line x1=\"{}\" y1=\"{}\" x2=\"{}\" y2=\"{}\" stroke=\"#ec792a\" stroke-width=\"3\"/>",
            25.5, line_y2, line_x2, chart_height
        )
        .as_str(),
    );

    g.middle
        .push_str("<text y=\"6\" x=\"25.5\" fill=\"#6f6f6f\">");
    g.middle
        .push_str(format!("Start backlog size: {}", sprint.start_story_points).as_str());
    g.middle.push_str("</text>");

    g.middle
        .push_str("<text y=\"26\" x=\"25.5\" fill=\"#6f6f6f\">");
    g.middle
        .push_str(format!("Sprint length: {} days", sprint.days).as_str());
    g.middle.push_str("</text>");

    g.middle
        .push_str("<text y=\"46\" x=\"25.5\" fill=\"#6f6f6f\">");
    g.middle.push_str(
        format!(
            "Sprint {}% completed",
            round_to_two_decimals(
                round_to_two_decimals(sprint.burned_points / sprint.start_story_points) * 100.0
            )
        )
        .as_str(),
    );
    g.middle.push_str("</text>");

    g.middle
        .push_str("<text y=\"6\" x=\"300\" fill=\"#6f6f6f\">");
    g.middle
        .push_str(format!("Backlog size: {}", sprint.story_points).as_str());
    g.middle.push_str("</text>");

    g.middle
        .push_str("<text y=\"26\" x=\"300\" fill=\"#6f6f6f\">");
    g.middle
        .push_str(format!("{} points burned", sprint.burned_points).as_str());
    g.middle.push_str("</text>");

    if sprint.days_left > -1 {
        g.middle
            .push_str("<text y=\"46\" x=\"300\" fill=\"#6f6f6f\">");
        g.middle
            .push_str(format!("{} working days left", sprint.days_left).as_str());
        g.middle.push_str("</text>");
    }

    //Team name + sprint #
    g.middle.push_str(
        format!(
            "<text y=\"500\" x=\"{}\" fill=\"#6f6f6f\" text-anchor=\"middle\" style=\"font-size: 1.5em\">",
            (team_text_pos / 2.0).round()
        )
        .as_str(),
    );
    g.middle
        .push_str(format!("{} - {}", team, sprint.name).as_str());
    g.middle.push_str("</text>");

    g.middle.push_str("</g>");

    format!("{}", g)
}

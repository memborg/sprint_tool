#[derive(Debug, Clone)]
pub struct Bucket {
    pub max: f64,
    pub min: f64,
    pub runs: i32,
}


use crate::config::*;
use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE};

#[derive(Debug, Deserialize)]
pub struct Href {
    pub href: String,
}

#[derive(Debug, Deserialize)]
pub struct Links {
    pub project: Href,
    pub team: Href,
    #[serde(rename = "teamIterations")]
    pub team_iterations: Href,
    #[serde(rename = "teamFieldValues")]
    pub team_field_values: Href,
}

#[derive(Debug, Deserialize)]
pub struct TeamSettings {
    pub url: String,
    #[serde(rename = "_links")]
    pub links: Links,
}

fn construct_headers() -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers
}

pub fn get_team_settings(team: &str) -> Result<TeamSettings, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();

    let url = reqwest::Url::parse(format!("https://dev.azure.com/logicmedia365/Logicmedia/{}/_apis/work/teamsettings?api-version=5.1", team).as_str()).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.username, Some(config.password))
        .send()
        .expect("Failed to send request");

    response.json::<TeamSettings>()
}

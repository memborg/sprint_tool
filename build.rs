use std::path::Path;
extern crate winres;

fn main() {
    if cfg!(target_os = "windows") {
        let path = Path::new("assets").join("logicmedia.ico");

        let mut res = winres::WindowsResource::new();
        res.set_icon(path.into_os_string().as_os_str().to_str().unwrap());
        res.compile().unwrap();
    }
}

use crate::config::*;
use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE};
use std::fmt;

#[derive(Clone, Debug, Deserialize)]
pub struct DevOpsTeam {
    pub id: String,
    pub name: String,
    pub url: String,
    #[serde(rename = "projectName")]
    pub project_name: String,
    #[serde(rename = "projectId")]
    pub project_id: String,
}

#[derive(Debug, Deserialize)]
pub struct Value {
    pub value: Vec<DevOpsTeam>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct TogglTeam {
    id: usize,
    name: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Team {
    pub id: String,
    pub name: String,
}

pub fn get_teams(strategi: &str) -> Vec<Team> {
    match strategi {
        "bp" => {
            if let Ok(teams) = get_toggl_teams() {
                return teams
                    .iter()
                    .map(|x| Team {
                        id: x.id.to_string(),
                        name: x.name.clone(),
                    })
                    .collect();
            }
        }
        _ => {
            if let Ok(value) = get_value() {
                return value
                    .value
                    .iter()
                    .map(|x| Team {
                        id: x.id.clone(),
                        name: x.name.clone(),
                    })
                    .collect();
            }
        }
    }

    Vec::new()
}

pub fn get_team(teams: Vec<Team>, i: usize) -> Team {
    teams[i].clone()
}

fn construct_headers() -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers
}

fn get_value() -> Result<Value, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();

    let url = reqwest::Url::parse("https://dev.azure.com/logicmedia365/_apis/projects/Logicmedia/teams?api-version=6.0-preview.3").expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.username, Some(config.password))
        .send()
        .expect("Failed to send request");

    response.json::<Value>()
}

fn get_toggl_teams() -> Result<Vec<TogglTeam>, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();

    let str_url = format!(
        "https://api.track.toggl.com/api/v8/workspaces/{}/groups",
        get_workspace_id()
    );

    let url = reqwest::Url::parse(&str_url).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.toggl, Some("api_token"))
        .send()
        .expect("Failed to send request");

    response.json::<Vec<TogglTeam>>()
}

impl fmt::Display for Team {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

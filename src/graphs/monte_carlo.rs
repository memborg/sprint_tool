use crate::bucket::Bucket;
use crate::graphs::*;
use crate::tools::*;

pub fn create_mc_svg(
    team: &str,
    buckets: Vec<Bucket>,
    avg: f64,
    median: f64,
    sprint_len: usize,
    chance: f64,
    completed_day: f64,
    backlog_size: i32,
    working_days: i32,
) -> String {
    let canvas_height = 500;
    let canvas_width = 960;
    let chart_height = 350;

    let mut g = Graph::new(canvas_height, canvas_width);

    g.middle.push_str("<g transform=\"translate(40,20)\">");

    g.middle.push_str(
        format!(
            "<g class=\"x axis\" transform=\"translate(0,{})\">",
            chart_height
        )
        .as_str(),
    );

    let mut x_pos: f64 = -38.5;
    let x_dist: f64 = 64.0;
    for b in buckets.iter() {
        x_pos = round_to_two_decimals(x_pos + x_dist);
        g.middle.push_str(
            format!(
                "<g class=\"tick\" transform=\"translate({},0)\" style=\"opacity: 1.0\">",
                x_pos
            )
            .as_str(),
        );
        g.middle.push_str("<line y2=\"6\" x2=\"0\"></line>");
        g.middle.push_str(
            "<text dy=\".71em\" y=\"9\" x=\"0\" fill=\"#6f6f6f\" text-anchor=\"middle\">",
        );
        g.middle.push_str(format!("{}", b.max).as_str());
        g.middle.push_str("</text>");
        g.middle.push_str("</g>");
    }
    x_pos = round_to_two_decimals(x_pos + x_dist).round();

    g.middle.push_str(
        format!(
            "<path class=\"domain\" d=\"M0,3V0H{}V3\" fill=\"#dadada\"></path>",
            x_pos
        )
        .as_str(),
    );

    g.middle.push_str("</g>"); //x axis end

    g.middle
        .push_str(format!("<text y=\"{}\" x=\"{}\" fill=\"#6f6f6f\">", 370, x_pos).as_str());
    g.middle.push_str("Days");
    g.middle.push_str("</text>");

    g.middle.push_str("<g class=\"y axis\">"); //y axis start

    let mut y_pos = chart_height;
    let mut y_start = 0;
    let y_dist = 50; //distance between ticks along the y axis
    loop {
        g.middle.push_str(
            format!(
                "<g class=\"tick\" transform=\"translate(0,{})\" style=\"opacity: 1;\">",
                y_pos
            )
            .as_str(),
        );
        g.middle.push_str("<line x2=\"-6\" y2=\"0\"></line>");
        g.middle
            .push_str("<text dy=\".32em\" x=\"-9\" y=\"0\" fill=\"#6f6f6f\" text-anchor=\"end\">");
        g.middle.push_str(format!("{}", y_start).as_str());
        g.middle.push_str("</text>");
        g.middle.push_str("</g>");

        y_pos -= y_dist;
        y_start += y_dist;

        if y_pos == 0 {
            break;
        }
    }

    g.middle
        .push_str("<text y=\"15\" x=\"-37\" fill=\"#6f6f6f\">");
    g.middle.push_str("Runs");
    g.middle.push_str("</text>");

    g.middle.push_str(
        format!(
            "<path class=\"domain\" d=\"M-3,0H0V{}H-3\" fill=\"#dadada\"></path>",
            chart_height
        )
        .as_str(),
    );
    g.middle
        .push_str("<text y=\"6\" x=\"25.5\" fill=\"#6f6f6f\">");
    g.middle
        .push_str(format!("Backlog size: {}", backlog_size).as_str());
    g.middle.push_str("</text>");

    g.middle
        .push_str("<text y=\"26\" x=\"25.5\" fill=\"#6f6f6f\">");
    g.middle
        .push_str(format!("Working days: {}", working_days).as_str());
    g.middle.push_str("</text>");

    g.middle
        .push_str("<text y=\"6\" x=\"400\" fill=\"#6f6f6f\">");
    g.middle.push_str(
        format!(
            "Average story points is {} over {} sprints",
            avg, sprint_len
        )
        .as_str(),
    );
    g.middle.push_str("</text>");
    g.middle
        .push_str("<text y=\"26\" x=\"400\" fill=\"#6f6f6f\">");
    g.middle.push_str(
        format!(
            "Median story points is {} over {} sprints",
            median, sprint_len
        )
        .as_str(),
    );
    g.middle.push_str("</text>");

    g.middle
        .push_str("<text y=\"46\" x=\"400\" fill=\"#6f6f6f\">");
    g.middle
        .push_str(format!("Chance of success is {}% in one sprint", chance).as_str());
    g.middle.push_str("</text>");

    if chance < 99.0 {
        g.middle
            .push_str("<text y=\"66\" x=\"400\" fill=\"#6f6f6f\">");
        g.middle.push_str("No room for buffer");
        g.middle.push_str("</text>");
    }

    if chance >= 99.0 {
        let max_bucket = buckets
            .iter()
            .filter(|x| x.max <= 10.0)
            .max_by(|x, y| x.max.partial_cmp(&y.max).unwrap())
            .unwrap();

        let buffer = 100.0 - ((max_bucket.max / 10.0) * 100.0);
        let buffer = round_to_two_decimals(buffer);

        g.middle
            .push_str("<text y=\"66\" x=\"400\" fill=\"#6f6f6f\">");
        g.middle
            .push_str(format!("Buffer size: {}%", buffer).as_str());
        g.middle.push_str("</text>");
    }

    g.middle
        .push_str("<text y=\"86\" x=\"400\" fill=\"#6f6f6f\">");
    g.middle.push_str(
        format!(
            "Sprint forecasted to be completed after {} days",
            completed_day
        )
        .as_str(),
    );
    g.middle.push_str("</text>");

    g.middle.push_str("</g>"); //y axis end

    let x_dist = x_dist as i32;
    let mut rect_x = 10;
    for b in buckets.iter() {
        let rect_y = chart_height - b.runs;
        g.middle.push_str(
            format!(
                "<rect class=\"bar\" x=\"{}\" width=\"31\" y=\"{}\" height=\"{}\" fill=\"#4472c4\">",
                rect_x, rect_y, b.runs
            )
            .as_str(),
        );
        g.middle.push_str("</rect>");

        rect_x += x_dist;
    }

    g.middle.push_str(
        format!(
            "<text y=\"400\" x=\"{}\" fill=\"#6f6f6f\" text-anchor=\"middle\" style=\"font-size: 1.5em\">",
            (x_pos / 2.0).round()
        )
        .as_str(),
    );
    g.middle.push_str(team);
    g.middle.push_str("</text>");

    g.middle.push_str("</g>");

    format!("{}", g)
}

use crate::period::Period;
use crate::work_items::WorkItemType;

#[derive(Debug, Clone)]
pub struct PBIAge {
    pub dow: usize,
    pub age: usize,
    pub count: usize,
}

#[derive(Debug, Clone)]
pub struct PBIAgePlot {
    pub value: Vec<PBIAge>,
    pub count: usize,
    pub sub_type: WorkItemType,
}

#[derive(Debug, Clone)]
pub struct PBILeadTime {
    pub doy: usize,
    pub age: usize,
    pub count: usize,
}

#[derive(Debug, Clone)]
pub struct PBILeadTimePlot {
    pub value: Vec<PBILeadTime>,
    pub lines: Vec<PBILeadTimeLines>,
    pub count: usize,
    pub total: usize,
    pub sub_type: WorkItemType,
    pub period: Period,
}

#[derive(Debug, Clone)]
pub struct PBILeadTimeLines {
    pub count: usize,
    pub total: usize,
    pub age: usize,
    pub range: String,
    pub num: usize,
}

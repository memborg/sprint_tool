use crate::config::*;
use crate::period;
use crate::teamsettings::teamfieldvalues;
use crate::teamsettings::teamsettings;
use crate::work_items::*;
use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE};
use std::collections::HashMap;

#[derive(Default, Debug, Deserialize, Clone)]
pub struct QueryWorkItem {
    pub id: i32,
    pub url: String,
}

#[derive(Default, Debug, Deserialize, Clone)]
pub struct Query {
    #[serde(rename = "workItems")]
    pub work_items: Vec<QueryWorkItem>,
}

fn construct_headers() -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers
}

impl QueryWorkItem {
    pub fn new() -> QueryWorkItem {
        QueryWorkItem::default()
    }
}

impl Query {
    pub fn new() -> Query {
        Query::default()
    }
}

pub fn get_work_items(
    team: &str,
    sub_type: &WorkItemType,
    period: period::Period,
) -> Vec<WorkItem> {
    let str_areas = format_teamsettings(team);
    let start_date = period.start.unwrap().format("%Y-%m-%d").to_string();
    let end_date = period.end.unwrap().format("%Y-%m-%d").to_string();
    let wi_type = WorkItemType::get_string(sub_type);
    let sql = format!("Select [System.Id] From WorkItems Where [System.WorkItemType] = '{}' AND [State] <> 'Removed' AND [System.CreatedDate] >= '{}' AND [System.CreatedDate] <= '{}' {}", wi_type, start_date, end_date, str_areas);

    get(&sql)
}

pub fn get_work_items_count(team: &str, sub_type: &WorkItemType, period: period::Period) -> usize {
    let str_areas = format_teamsettings(team);
    let start_date = period.start.unwrap().format("%Y-%m-%d").to_string();
    let end_date = period.end.unwrap().format("%Y-%m-%d").to_string();
    let wi_type = WorkItemType::get_string(sub_type);
    let sql = format!("Select [System.Id] From WorkItems Where [System.WorkItemType] = '{}' AND [State] <> 'Removed' AND [System.CreatedDate] >= '{}' AND [System.CreatedDate] <= '{}' {}", wi_type, start_date, end_date, str_areas);

    get_count(&sql)
}

pub fn get_new_work_items(
    team: &str,
    sub_type: &WorkItemType,
    period: period::Period,
) -> Vec<WorkItem> {
    let str_areas = format_teamsettings(team);
    let start_date = period.start.unwrap().format("%Y-%m-%d").to_string();
    let end_date = period.end.unwrap().format("%Y-%m-%d").to_string();
    let wi_type = WorkItemType::get_string(sub_type);
    let sql = format!("Select [System.Id] From WorkItems Where [System.WorkItemType] = '{}' AND [State] = 'new' AND [System.CreatedDate] >= '{}' AND [System.CreatedDate] <= '{}' {}", wi_type, start_date, end_date, str_areas);

    get(&sql)
}

pub fn get_new_work_items_count(
    team: &str,
    sub_type: &WorkItemType,
    period: period::Period,
) -> usize {
    let str_areas = format_teamsettings(team);
    let start_date = period.start.unwrap().format("%Y-%m-%d").to_string();
    let end_date = period.end.unwrap().format("%Y-%m-%d").to_string();
    let wi_type = WorkItemType::get_string(sub_type);
    let sql = format!("Select [System.Id] From WorkItems Where [System.WorkItemType] = '{}' AND [State] = 'new' AND [System.CreatedDate] >= '{}' AND [System.CreatedDate] <= '{}' {}", wi_type, start_date, end_date, str_areas);

    get_count(&sql)
}

pub fn get_closed_work_items(
    team: &str,
    sub_type: &WorkItemType,
    period: period::Period,
) -> Vec<WorkItem> {
    let str_areas = format_teamsettings(team);
    let start_date = period.start.unwrap().format("%Y-%m-%d").to_string();
    let end_date = period.end.unwrap().format("%Y-%m-%d").to_string();
    let wi_type = WorkItemType::get_string(sub_type);
    let sql = format!("Select [System.Id] From WorkItems Where [System.WorkItemType] = '{}' AND [State] = 'Closed' AND [Microsoft.VSTS.Common.ClosedDate] >= '{}' AND [Microsoft.VSTS.Common.ClosedDate] <= '{}' {}", wi_type, start_date, end_date, str_areas);

    get(&sql)
}

fn get_count(sql: &str) -> usize {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let url = reqwest::Url::parse(
        "https://dev.azure.com/logicmedia365/Logicmedia/_apis/wit/wiql?api-version=5.1",
    )
    .expect("Failed to parse url");

    let mut query = HashMap::new();
    query.insert("query", sql);

    let response = client
        .post(url)
        .headers(construct_headers())
        .basic_auth(config.username, Some(config.password))
        .json(&query)
        .send()
        .expect("Failed to send request");

    let mut count = 0;

    if let Ok(q) = response.json::<Query>() {
        count = q.work_items.len();
    }

    count
}

fn get(sql: &str) -> Vec<WorkItem> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();
    let url = reqwest::Url::parse(
        "https://dev.azure.com/logicmedia365/Logicmedia/_apis/wit/wiql?api-version=5.1",
    )
    .expect("Failed to parse url");

    let mut query = HashMap::new();
    query.insert("query", sql);

    let response = client
        .post(url)
        .headers(construct_headers())
        .basic_auth(config.username, Some(config.password))
        .json(&query)
        .send()
        .expect("Failed to send request");

    let mut wis: Vec<WorkItem> = Vec::new();

    if let Ok(q) = response.json::<Query>() {
        for item in q.work_items {
            let wi = get_work_item_simple(item.id);
            wis.push(wi);
        }
    }

    wis
}

fn format_teamsettings(team: &str) -> String {
    let mut str_areas = String::new();

    if let Ok(team_settings) = teamsettings::get_team_settings(team) {
        let mut areas: Vec<String> = vec![];
        if let Ok(team_field_values) =
            teamfieldvalues::get_team_field_values(&team_settings.links.team_field_values.href)
        {
            for area in team_field_values.values {
                areas.push(format!("[System.AreaPath] = '{}'", area.value));
            }
        }

        if !areas.is_empty() {
            str_areas = format!(" AND ({}) ", areas.join(" OR "));
        }
    }

    str_areas
}

extern crate chrono;
use chrono::prelude::*;
use chrono::Duration;
use console::Term;
use dialoguer::{theme::ColorfulTheme, Input, Select};
#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate savefile_derive;

extern crate reqwest;
extern crate serde_json;

pub mod bucket;
pub mod cfd_item;
pub mod config;
pub mod graphs;
pub mod iterations;
pub mod period;
pub mod plot;
pub mod queries;
pub mod sprints;
pub mod teams;
pub mod teamsettings;
pub mod toggl;
pub mod tools;
pub mod work_items;

use crate::tools::*;
use rand::{self, Rng};
use std::env;
use std::fs::File;
use std::io::prelude::*;

#[derive(Debug)]
pub struct Params {
    pub backlog_size: usize,
    pub team: String,
    pub toggl_team: String,
    pub sprint_name: String,
    pub sprint_len: usize,
    pub chart_type: String,
    pub sub_type: String,
    pub period: String,
}

fn main() -> std::io::Result<()> {
    let args: Vec<_> = env::args().collect();

    if args.len() > 1 {
        cli_params(&args)?;
    } else {
        config::run_setup()?;

        let mut backlog_size: usize = 100;
        let mut team = "Wilhelm".into();
        let mut toggl_team = "APPA".into();
        let mut sprint_name = "Sprint 010".into();
        let mut sprint_len: usize = 10;
        let mut chart_type = "cfd".into();
        let mut sub_type = "bug".into();
        let mut period = "20q1".into();

        let types = graphs::get_graph_types();

        let mut t = teams::get_teams(&String::new());
        t.sort_by(|a, b| a.name.cmp(&b.name));

        let t_selection = Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Select DevOps team")
            .items(&t)
            .interact_on_opt(&Term::stderr())?;

        if let Some(i) = t_selection {
            let t = teams::get_team(t, i);
            team = t.name;
        }

        let c_selection = Select::with_theme(&ColorfulTheme::default())
            .with_prompt("Select chart type")
            .items(&types)
            .interact_on_opt(&Term::stderr())?;

        if let Some(i) = c_selection {
            let c = graphs::get_graph_type(types, i);
            chart_type = c.key;
        }

        match chart_type.as_str() {
            "bc" => {}
            "bp" => {
                let mut t = teams::get_teams(&chart_type);
                t.sort_by(|a, b| a.name.cmp(&b.name));

                let t_selection = Select::with_theme(&ColorfulTheme::default())
                    .with_prompt("Select Toggl team")
                    .items(&t)
                    .interact_on_opt(&Term::stderr())?;

                if let Some(i) = t_selection {
                    let t = teams::get_team(t, i);
                    toggl_team = t.id;
                }
            }
            "cfd" => {
                let sub_types = get_subtypes();

                let s_selection = Select::with_theme(&ColorfulTheme::default())
                    .with_prompt("Select work item type")
                    .items(&sub_types)
                    .default(0)
                    .interact_on_opt(&Term::stderr())?;
                if let Some(i) = s_selection {
                    sub_type = get_subtype(&sub_types, i);
                }

                let input: String = Input::with_theme(&ColorfulTheme::default())
                    .with_prompt("Type in period. Example: 21q1 for first quarter of 2021. 21h1 for first half of 2021. 21m1 for first month of 2021.")
                    .with_initial_text("21q1")
                    .default("21q1".into())
                    .allow_empty(false)
                    .interact_text()?;

                period = input.parse::<String>().unwrap();
            }
            "mc" => {
                let input: String = Input::with_theme(&ColorfulTheme::default())
                    .with_prompt("Backlog size to forecast?")
                    .with_initial_text("40")
                    .interact_text()?;

                backlog_size = input.parse::<usize>().unwrap();

                let input: String = Input::with_theme(&ColorfulTheme::default())
                    .with_prompt("Sprint length?")
                    .with_initial_text("10")
                    .default("10".into())
                    .allow_empty(false)
                    .interact_text()?;

                sprint_len = input.parse::<usize>().unwrap();
            }
            "oldbc" => {
                let mut it = iterations::get_iterations_value(&team);

                it.sort_by(|a, b| b.attributes.start_date.cmp(&a.attributes.start_date));

                let it_selection = Select::with_theme(&ColorfulTheme::default())
                    .with_prompt("Select iteration")
                    .items(&it)
                    .interact_on_opt(&Term::stderr())?;

                if let Some(i) = it_selection {
                    let it = iterations::get_iteration_by_i(&it, i);
                    sprint_name = it.name;
                }
            }
            "pbiage" | "leadtime" => {
                let sub_types = get_subtypes();

                let s_selection = Select::with_theme(&ColorfulTheme::default())
                    .with_prompt("Select work item type")
                    .items(&sub_types)
                    .default(0)
                    .interact_on_opt(&Term::stderr())?;
                if let Some(i) = s_selection {
                    sub_type = get_subtype(&sub_types, i);
                }
            }
            _ => {
                println!("Unknow chart type: {}", chart_type);
            }
        };

        let p = Params {
            chart_type,
            team,
            toggl_team,
            backlog_size,
            sprint_name,
            sprint_len,
            sub_type,
            period,
        };

        choose_strategi(&p)?;
    }

    Ok(())
}

fn cli_params(args: &[String]) -> std::io::Result<()> {
    let chart_type = args[1].as_str();
    let team = &args[2];
    let mut toggl_team = "APPA";
    let mut backlog_size: usize = 100;
    let mut sprint_name = "Sprint 010";
    let mut sprint_len: usize = 10;
    let mut sub_type = "bug";
    let mut period = "20Q1";

    match chart_type {
        "bc" => burndown(team)?,
        "bp" => {
            if args.len() > 3 {
                toggl_team = &args[3];
            }

            bugpie(team, toggl_team)?
        }
        "cfd" => {
            if args.len() > 4 {
                sub_type = &args[3];
                period = &args[4];
            }
        }
        "mc" => {
            if args.len() > 4 {
                backlog_size = args[3].parse().unwrap();
                sprint_len = args[4].parse().unwrap();
            }
        }
        "oldbc" => {
            if args.len() > 3 {
                sprint_name = &args[3];
            }
        }
        "pbiage" => {
            if args.len() > 3 {
                sub_type = &args[3];
            }
        }
        "leadtime" => {
            if args.len() > 3 {
                sub_type = &args[3];
            }
        }
        _ => {
            println!("Unknow chart type: {}", chart_type);
        }
    }

    let p = Params {
        backlog_size,
        chart_type: chart_type.into(),
        team: team.into(),
        toggl_team: toggl_team.into(),
        sprint_name: sprint_name.into(),
        sprint_len,
        sub_type: sub_type.into(),
        period: period.into(),
    };

    choose_strategi(&p)?;

    Ok(())
}

fn choose_strategi(p: &Params) -> std::io::Result<()> {
    match p.chart_type.as_str() {
        "bc" => burndown(&p.team)?,
        "bp" => bugpie(&p.team, &p.toggl_team)?,
        "cfd" => {
            cfd(&p.team, &p.sub_type, &p.period)?;
        }
        "mc" => {
            monte_carlo(&p.team, p.backlog_size as i32, p.sprint_len as i32)?;
        }
        "oldbc" => old_burndown(&p.team, &p.sprint_name)?,
        "pbiage" => pbi_age(&p.team, &p.sub_type)?,
        "leadtime" => lead_time(&p.team, &p.sub_type)?,
        _ => {
            println!("Unknow chart type: {}", p.chart_type);
        }
    }

    Ok(())
}

fn burndown(team: &str) -> std::io::Result<()> {
    let sprint = sprints::get_current_sprint(team);

    let filename = format!("burndown-{}-{}.svg", team, sprint.name);

    let mut file = File::create(&filename)?;
    file.write_all(
        graphs::burndown::create_bc_svg(team, sprint)
            .into_bytes()
            .as_slice(),
    )?;

    open_file(&filename);

    Ok(())
}

fn old_burndown(team: &str, sprint_name: &str) -> std::io::Result<()> {
    let sprint = sprints::get_sprint(team, sprint_name);

    let filename = format!("burndown-{}-{}.svg", team, sprint.name);
    let mut file = File::create(&filename)?;
    file.write_all(
        graphs::burndown::create_bc_svg(team, sprint)
            .into_bytes()
            .as_slice(),
    )?;

    open_file(&filename);

    Ok(())
}

fn cfd(team: &str, sub_type: &str, period: &str) -> std::io::Result<()> {
    let period_obj = match period::get_period(period) {
        Ok(obj) => obj,
        Err(e) => panic!("{}", e),
    };

    let wi_type = work_items::WorkItemType::get_enum(sub_type);

    let wis = queries::get_work_items(team, &wi_type, period_obj.clone());

    let mut cfd_col = cfd_item::CFDCollection::new();
    cfd_col.period = period_obj;
    cfd_col.work_items = wis.clone();

    for d in 0..cfd_col.period.days {
        let next_date = cfd_col.period.start.unwrap() + Duration::days(d);

        cfd_col.lead_time.push(cfd_item::CFDItem {
            date: Some(next_date),
            count: 0,
        });
        cfd_col.cycle_time.push(cfd_item::CFDItem {
            date: Some(next_date),
            count: 0,
        });

        cfd_col.resolved.push(cfd_item::CFDItem {
            date: Some(next_date),
            count: 0,
        });

        cfd_col.closed.push(cfd_item::CFDItem {
            date: Some(next_date),
            count: 0,
        });
    }

    for wi in wis {
        if let Some(date) = wi.fields.created_date {
            let cd = Utc.ymd(date.year(), date.month(), date.day());
            if let Some(i) = cfd_col.lead_time.iter().position(|x| x.date.unwrap() == cd) {
                cfd_col.lead_time[i].count += 1;
            }
        }

        if let Some(date) = wi.fields.activated_date {
            let cd = Utc.ymd(date.year(), date.month(), date.day());
            if let Some(i) = cfd_col
                .cycle_time
                .iter()
                .position(|x| x.date.unwrap() == cd)
            {
                cfd_col.cycle_time[i].count += 1;
            }
        }

        if let Some(date) = wi.fields.resolved_date {
            let cd = Utc.ymd(date.year(), date.month(), date.day());
            if let Some(i) = cfd_col.resolved.iter().position(|x| x.date.unwrap() == cd) {
                cfd_col.resolved[i].count += 1;
            }
        }

        if let Some(date) = wi.fields.closed_date {
            let cd = Utc.ymd(date.year(), date.month(), date.day());
            if let Some(i) = cfd_col.closed.iter().position(|x| x.date.unwrap() == cd) {
                cfd_col.closed[i].count += 1;
            }
        }
    }

    cfd_col.lead_time.sort_by_key(|x| x.date.unwrap());
    cfd_col.cycle_time.sort_by_key(|x| x.date.unwrap());
    cfd_col.resolved.sort_by_key(|x| x.date.unwrap());
    cfd_col.closed.sort_by_key(|x| x.date.unwrap());

    for i in 1..cfd_col.lead_time.len() {
        let prev = (i - 1) as usize;
        cfd_col.lead_time[i].count += cfd_col.lead_time[prev].count;
    }

    for i in 1..cfd_col.cycle_time.len() {
        let prev = (i - 1) as usize;
        cfd_col.cycle_time[i].count += cfd_col.cycle_time[prev].count;
    }

    for i in 1..cfd_col.resolved.len() {
        let prev = (i - 1) as usize;
        cfd_col.resolved[i].count += cfd_col.resolved[prev].count;
    }

    for i in 1..cfd_col.closed.len() {
        let prev = (i - 1) as usize;
        cfd_col.closed[i].count += cfd_col.closed[prev].count;
    }

    let filename = format!("cfd-{}-{}-{}.svg", team, sub_type, period);
    let mut file = File::create(&filename)?;
    file.write_all(
        graphs::cfd::create_cfd_svg(team, &wi_type, cfd_col)
            .into_bytes()
            .as_slice(),
    )?;

    open_file(&filename);

    Ok(())
}

fn pbi_age(team: &str, sub_type: &str) -> std::io::Result<()> {
    let wi_type = work_items::WorkItemType::get_enum(sub_type);

    let period_obj = period::Period {
        start: Some(Utc.ymd(2020, 1, 1)),
        end: Some(Utc.ymd(2030, 12, 31)),
        name: String::from("Very long period"),
        days: 9999,
        modulo: 3,
    };

    let mut plots: Vec<plot::PBIAge> = Vec::new();
    let age_range: i32 = 7; //Take into account if the bubbles are to close and collapse them into one

    let wis = queries::get_new_work_items(team, &wi_type, period_obj);
    let count = wis.len();
    for wi in wis {
        let dow = wi
            .fields
            .created_date
            .unwrap()
            .weekday()
            .number_from_monday() as usize;
        let age = wi.fields.lead_time as i32 / 24;

        //Take into account if the bubbles are to close and collapse them into one
        if let Some(p) = plots.iter().position(|x| {
            x.dow == dow && x.age as i32 - age_range <= age && x.age as i32 + age_range >= age
        }) {
            plots[p].count += 1;
        } else {
            plots.push(plot::PBIAge {
                dow,
                age: age as usize,
                count: 1,
            });
        }
    }

    let plot = plot::PBIAgePlot {
        value: plots,
        count,
        sub_type: wi_type,
    };

    let filename = format!("pbiage-{}-{}.svg", team, sub_type);

    let mut file = File::create(&filename)?;
    file.write_all(
        graphs::pbiage::create_pbiage_svg(team, &plot)
            .into_bytes()
            .as_slice(),
    )?;

    open_file(&filename);

    Ok(())
}

fn lead_time(team: &str, sub_type: &str) -> std::io::Result<()> {
    let wi_type = work_items::WorkItemType::get_enum(sub_type);

    let today = Utc::today();

    let start = Utc.ymd(today.year(), 1, 1);
    let end = Utc.ymd(today.year(), 12, 31);
    let start_time = Utc.ymd(today.year(), 1, 1).and_hms(0, 0, 0);

    let period_obj = period::Period {
        start: Some(start),
        end: Some(end),
        name: String::from("Current year"),
        days: (end - start).num_days(),
        modulo: 1,
    };

    let mut plots: Vec<plot::PBILeadTime> = Vec::new();
    let age_range: i32 = 7; //Bubble size. Take into account if the bubbles are to close and collapse them into one

    let wis = queries::get_closed_work_items(team, &wi_type, period_obj.clone());
    let count = wis.len();

    for wi in wis {
        let doy = (wi.fields.closed_date.unwrap() - start_time).num_days() as usize;
        let age = wi.fields.lead_time as i32 / 24;

        //Take into account if the bubbles are to close and collapse them into one
        if let Some(p) = plots.iter().position(|x| {
            x.doy == doy && x.age as i32 - age_range <= age && x.age as i32 + age_range >= age
        }) {
            plots[p].count += 1;
        } else {
            plots.push(plot::PBILeadTime {
                doy,
                age: age as usize,
                count: 1,
            });
        }
    }

    let mut plots2 = plots.clone();
    plots2.sort_by_key(|x| x.age);
    let middle_pos = plots2.len() / 2;

    let (left, _) = plots2.split_at(middle_pos);
    let left_count = left.iter().map(|x| x.count).sum::<usize>();

    let prev_count = left_count;

    let mut lines: Vec<plot::PBILeadTimeLines> = vec![plot::PBILeadTimeLines {
        count: prev_count,
        total: left_count,
        age: left.iter().max_by_key(|x| x.age).unwrap().age,
        range: "0-50%".into(),
        num: 50,
    }];

    // let mut lines: Vec<plot::PBILeadTimeLines> = Vec::new();
    // lines.push(plot::PBILeadTimeLines {
    //     count: prev_count,
    //     total: left_count,
    //     age: left.iter().max_by_key(|x| x.age).unwrap().age,
    //     range: "0-50%".into(),
    //     num: 50,
    // });

    let middle_pos = (plots2.len() as f64 * 0.70) as usize;
    let (left, _) = plots2.split_at(middle_pos);
    let left_count = left.iter().map(|x| x.count).sum::<usize>();

    let prev_count = left_count - prev_count;

    lines.push(plot::PBILeadTimeLines {
        count: prev_count,
        total: left_count,
        age: left.iter().max_by_key(|x| x.age).unwrap().age,
        range: "50-70%".into(),
        num: 70,
    });

    let prev_count = left_count;

    let middle_pos = (plots2.len() as f64 * 0.85) as usize;
    let (left, _) = plots2.split_at(middle_pos);
    let left_count = left.iter().map(|x| x.count).sum::<usize>();

    let prev_count = left_count - prev_count;

    lines.push(plot::PBILeadTimeLines {
        count: prev_count,
        total: left_count,
        age: left.iter().max_by_key(|x| x.age).unwrap().age,
        range: "70-85%".into(),
        num: 85,
    });
    let prev_count = left_count;

    let middle_pos = (plots2.len() as f64 * 0.95) as usize;
    let (left, _) = plots2.split_at(middle_pos);
    let left_count = left.iter().map(|x| x.count).sum::<usize>();

    let prev_count = left_count - prev_count;

    lines.push(plot::PBILeadTimeLines {
        count: prev_count,
        total: left_count,
        age: left.iter().max_by_key(|x| x.age).unwrap().age,
        range: "95-95%".into(),
        num: 95,
    });

    let plot = plot::PBILeadTimePlot {
        value: plots,
        lines,
        count,
        total: queries::get_work_items_count(team, &wi_type, period_obj.clone()),
        sub_type: wi_type,
        period: period_obj,
    };

    let filename = format!("pbileadtime-{}-{}.svg", team, sub_type);

    let mut file = File::create(&filename)?;
    file.write_all(
        graphs::pbileadtime::create_pbileadtime_svg(team, &plot)
            .into_bytes()
            .as_slice(),
    )?;

    open_file(&filename);

    Ok(())
}

fn monte_carlo(team: &str, backlog_size: i32, sprint_len: i32) -> std::io::Result<()> {
    let mut sprints: Vec<sprints::Sprint> = Vec::new();
    let mut iters = iterations::get_past_iterations_in_range(team, sprint_len - 2, sprint_len + 2);

    iters.value.sort_by(|a, b| {
        b.attributes
            .start_date
            .unwrap()
            .cmp(&a.attributes.start_date.unwrap())
    }); //Sort sprint byt start date descending

    iters.value = iters.value.drain(..6).collect(); //Work only on the 6 recent sprints.

    for iter in iters.value.iter() {
        if let Ok(iteration) = iterations::get_iteration_work_items(team, &iter.id) {
            let mut sprint = sprints::Sprint::new();
            sprint.name = iter.name.clone();
            sprint.days = iter.attributes.days;
            sprint.start_date = iter.attributes.start_date;

            for work_item in iteration.work_item_relations {
                let wi = work_items::get_work_item(team, work_item.target.id, &sprint);
                sprint.story_points += wi.fields.story_points;
            }

            sprint.days_per_point = iter.attributes.days as f64 / sprint.story_points;

            sprints.push(sprint);
        }
    }

    let avg: f64 = sprints.iter().map(|x| x.story_points).sum::<f64>() / sprints.len() as f64;
    let avg = round_to_two_decimals(avg);

    let mut sorted_sprints = sprints.clone();

    sorted_sprints.sort_by(|x, y| x.story_points.partial_cmp(&y.story_points).unwrap());

    let median = sorted_sprints.len() / 2;
    let median: f64 = sorted_sprints[median].story_points;

    let mut simulations: Vec<f64> = Vec::new();
    let simulation_runs = 1_000; //The higher the more accurate, but also slower.
    let buckets_num = sprint_len;
    let len = sprints.len();

    for _s in 0..simulation_runs {
        let mut days: f64 = 0.0;
        for _ in 0..backlog_size {
            let i = rand::thread_rng().gen_range(0, len);
            days += sprints[i].days_per_point;
        }

        let days = round_to_two_decimals(days);
        simulations.push(days);
    }

    let min_sim = simulations
        .iter()
        .min_by(|x, y| x.partial_cmp(y).unwrap())
        .unwrap();
    let max_sim = simulations
        .iter()
        .max_by(|x, y| x.partial_cmp(y).unwrap())
        .unwrap();
    let diff_sim = round_to_two_decimals(max_sim - min_sim);
    let step_size = round_to_two_decimals(diff_sim / buckets_num as f64);

    let buckets = init_buckets(*min_sim, buckets_num, step_size);
    let buckets = add_to_bucket(simulations, buckets);

    let chance = chance_of_success(&buckets);

    let completed_day = buckets.iter().max_by_key(|x| x.runs).unwrap();

    let filename = format!("monte-carlo-{}.svg", team);
    let mut file = File::create(&filename)?;
    file.write_all(
        graphs::monte_carlo::create_mc_svg(
            team,
            buckets.clone(),
            avg,
            median,
            sprints.len(),
            chance,
            completed_day.max,
            backlog_size,
            sprint_len,
        )
        .into_bytes()
        .as_slice(),
    )?;

    open_file(&filename);

    Ok(())
}

fn bugpie(team: &str, toggl_team: &str) -> std::io::Result<()> {
    let sprint = sprints::get_current_sprint(team);
    let details = toggl::report_details::get_details_report(
        toggl_team,
        sprint.start_date.unwrap(),
        sprint.end_date.unwrap(),
    );
    println!("{:?}", details);

    let filename = format!("bugpie-{}.svg", team);
    let mut file = File::create(&filename)?;
    file.write_all(
        graphs::bugpie::create_bp_svg(team, toggl_team, sprint, details)
            .into_bytes()
            .as_slice(),
    )?;

    open_file(&filename);

    Ok(())
}

fn init_buckets(min: f64, num: i32, step: f64) -> Vec<bucket::Bucket> {
    let mut buckets: Vec<bucket::Bucket> = Vec::new();

    let mut _min = min;
    let mut _max = 0.0;
    for _i in 0..num {
        _max = round_to_two_decimals(_min + step);
        buckets.push(bucket::Bucket {
            min: _min,
            max: _max,
            runs: 0,
        });
        _min = _max;
        _max = 0.0;
    }

    buckets
}

fn add_to_bucket(simulations: Vec<f64>, mut buckets: Vec<bucket::Bucket>) -> Vec<bucket::Bucket> {
    for e in simulations.iter() {
        if let Some(i) = buckets.iter().position(|x| x.min <= *e && x.max >= *e) {
            buckets[i].runs += 1
        }
    }

    buckets
}

fn chance_of_success(buckets: &[bucket::Bucket]) -> f64 {
    let num_buckets = buckets.len();

    let count: i32 = buckets
        .iter()
        .filter(|x| x.max <= num_buckets as f64)
        .map(|x| x.runs)
        .sum();

    round_to_two_decimals(count as f64 / num_buckets as f64)
}

fn get_subtypes() -> Vec<String> {
    vec!["Bug".into(), "UserStory".into()]
}

fn get_subtype(sub_types: &[String], i: usize) -> String {
    sub_types[i].clone()
}

fn open_file(filename: &str) {
    let exe = std::env::current_exe().unwrap();
    let path = exe.parent().unwrap();
    let path = path.join(filename);
    let path = format!("file://{}", path.into_os_string().into_string().unwrap());

    open::that(path).unwrap();
}

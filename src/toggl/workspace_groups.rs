use crate::config::*;
use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE};
use std::fmt;

#[derive(Debug, Deserialize)]
pub struct WorkspaceGroups {
    id: usize,
    name: String,
}

impl fmt::Display for WorkspaceGroups {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

fn construct_headers() -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers
}

pub fn get_workspace_groups() -> Result<WorkspaceGroups, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();

    let str_url = format!(
        "https://api.track.toggl.com/api/v8/workspaces/{}/groups",
        get_workspace_id()
    );

    let url = reqwest::Url::parse(str_url.as_str()).expect("Failed to parse url");

    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.toggl, Some("api_token"))
        .send()
        .expect("Failed to send request");

    response.json::<WorkspaceGroups>()
}

use crate::config::*;
use chrono::prelude::*;
use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE};
use std::{thread, time};

#[derive(Debug, Deserialize, Clone)]
pub struct TogglDetailEntry {
    pub id: u64,
    pub uid: u64,
    pub pid: Option<u64>,
    pub start: Option<DateTime<Utc>>,
    pub end: Option<DateTime<Utc>>,
    pub update: Option<DateTime<Utc>>,
    pub dur: u64,
    pub user: String,
    pub tags: Vec<String>,
    pub project: Option<String>,
    pub task: Option<String>,
}

#[derive(Debug, Deserialize, Clone)]
pub struct TogglDetailsReport {
    pub total_grand: u64,
    pub total_count: usize,
    pub per_page: usize,
    pub data: Vec<TogglDetailEntry>,
}

#[derive(Default, Debug, Clone)]
pub struct DetailsReport {
    pub total: u64,
    pub development: u64,
    pub bugs: u64,
    pub support: u64,
    pub misc: u64,
    pub agile: u64,
}

impl DetailsReport {
    pub fn new() -> DetailsReport {
        DetailsReport::default()
    }
}

pub fn get_details_report(
    group: &str,
    since: DateTime<Utc>,
    until: DateTime<Utc>,
) -> DetailsReport {
    let agile_projects = [
        155358200, //Agile - Incrementing
        155358223, //Agile - Process
    ];

    if let Some(details) = get_toggl_details(group, since, until) {
        let mut d = DetailsReport::new();

        d.total = details.total_grand;
        d.development = details
            .data
            .iter()
            .filter(|x| x.tags.contains(&"Development".into()))
            .map(|x| x.dur)
            .sum();
        d.bugs = details
            .data
            .iter()
            .filter(|x| x.tags.contains(&"Bugs".into()))
            .map(|x| x.dur)
            .sum();
        d.support = details
            .data
            .iter()
            .filter(|x| x.tags.contains(&"Support".into()))
            .map(|x| x.dur)
            .sum();

        for data in details.data {
            if let Some(pid) = data.pid {
                if agile_projects.contains(&pid) {
                    d.agile += data.dur;
                }
            } else if data.tags.is_empty() {
                d.misc += data.dur;
            }
        }

        return d;
    }

    DetailsReport::new()
}

pub fn get_toggl_details(
    group: &str,
    since: DateTime<Utc>,
    until: DateTime<Utc>,
) -> Option<TogglDetailsReport> {
    let one_sec = time::Duration::from_millis(1001);
    let mut page = 1;

    if let Ok(mut details) = request_details(group, since, until, page) {
        if details.data.len() > details.per_page {
            loop {
                page += 1;
                if let Ok(mut d) = request_details(group, since, until, page) {
                    details.data.append(&mut d.data);
                }

                if details.data.len() > details.per_page {
                    break;
                }

                thread::sleep(one_sec);
            }
        }

        return Some(details);
    }

    None
}

fn construct_headers() -> HeaderMap {
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers
}

fn request_details(
    group: &str,
    since: DateTime<Utc>,
    until: DateTime<Utc>,
    page: usize,
) -> Result<TogglDetailsReport, reqwest::Error> {
    let config = get_config();
    let client = reqwest::blocking::Client::new();

    let since = since.format("%Y-%m-%d").to_string();
    let until = until.format("%Y-%m-%d").to_string();

    let str_url = format!("https://api.track.toggl.com/reports/api/v2/details?user_agent={}&workspace_id={}&members_of_group_ids={}&since={}&until={}&page={}", config.username, get_workspace_id(), group, since, until, page);

    let url = reqwest::Url::parse(&str_url).expect("Failed to parse url");
    let response = client
        .get(url)
        .headers(construct_headers())
        .basic_auth(config.toggl, Some("api_token"))
        .send()
        .expect("Failed to send request");

    response.json::<TogglDetailsReport>()
}

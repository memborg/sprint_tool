use crate::iterations;
use crate::work_items;
use chrono::prelude::*;

#[derive(Debug, Clone)]
pub struct Progress {
    pub closed: f64,
    pub remaining: f64,
    pub total: f64,
}

#[derive(Debug, Clone)]
pub struct Sprint {
    pub name: String,
    pub days: i32,
    pub days_left: i32,
    pub days_progress: i32,
    pub start_story_points: f64,
    pub story_points: f64,
    pub days_per_point: f64,
    pub burned_points: f64,
    pub progress: Vec<Progress>,
    pub closed_points: Vec<f64>,
    pub start_date: Option<DateTime<Utc>>,
    pub end_date: Option<DateTime<Utc>>,
}

impl Default for Sprint {
    fn default() -> Sprint {
        Sprint {
            name: String::new(),
            days: 0,
            days_progress: 0,
            days_left: 0,
            start_story_points: 0.0,
            story_points: 0.0,
            days_per_point: 0.0,
            burned_points: 0.0,
            closed_points: Vec::new(),
            progress: Vec::new(),
            start_date: None,
            end_date: None,
        }
    }
}

impl Sprint {
    pub fn new() -> Sprint {
        Sprint::default()
    }
}

impl Default for Progress {
    fn default() -> Progress {
        Progress {
            closed: 0.0,
            remaining: 0.0,
            total: 0.0,
        }
    }
}

impl Progress {
    pub fn new() -> Progress {
        Progress::default()
    }
}

pub fn get_sprint(team: &str, sprint_name: &str) -> Sprint {
    let iterations = iterations::get_iteration(team, sprint_name);

    handle_iteration(team, iterations)
}

pub fn get_current_sprint(team: &str) -> Sprint {
    let iterations = iterations::get_current_iteration(team);

    handle_iteration(team, iterations)
}

fn handle_iteration(team: &str, iteration: iterations::Iterations) -> Sprint {
    let mut sprint = Sprint::new();
    if let Some(iter) = iteration.value.get(0) {
        if let Ok(iteration) = iterations::get_iteration_work_items(team, &iter.id) {
            sprint = Sprint::new();
            sprint.name = iter.name.clone();
            sprint.start_date = iter.attributes.start_date;
            sprint.end_date = iter.attributes.finish_date;
            sprint.days = iter.attributes.days;
            sprint.days_progress = iter.attributes.days_progress;
            sprint.days_left = sprint.days - sprint.days_progress;
            sprint.closed_points = vec![0.0; iter.attributes.days as usize];
            sprint.progress = vec![Progress::new(); iter.attributes.days as usize];

            let mut progress_max = sprint.days_progress;
            if sprint.days_progress > sprint.days {
                progress_max = sprint.days;
            }

            for work_item in iteration.work_item_relations {
                let wi = work_items::get_work_item(team, work_item.target.id, &sprint);

                if let work_items::WorkItemType::UserStory = wi.fields.work_item_type {
                    sprint.story_points += wi.fields.story_points;
                    let ad = wi.fields.added_day;

                    if wi.fields.added_day <= 0 {
                        sprint.start_story_points += wi.fields.story_points;
                        sprint.progress[0].total += wi.fields.story_points;
                    }

                    for i in 1..progress_max {
                        if ad < progress_max {
                            sprint.progress[i as usize].total += wi.fields.story_points;
                        }
                    }

                    if wi.fields.closed_date.is_some() {
                        if wi.fields.closed_day < sprint.days {
                            let cd = wi.fields.closed_day as usize;
                            if sprint.closed_points.get(cd).is_some() {
                                sprint.closed_points[cd] += wi.fields.story_points;
                            }

                            if sprint.progress.get(cd).is_some() {
                                sprint.progress[cd].closed += wi.fields.story_points;
                            }
                        }

                        if wi.fields.closed_day < sprint.days_progress
                            && wi.fields.closed_day < sprint.days
                        {
                            sprint.burned_points += wi.fields.story_points;
                        }
                    }
                }
            }
        }
    }

    sprint
}
